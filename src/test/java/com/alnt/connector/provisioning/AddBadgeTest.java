package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * 
 * @author soori
 *
 */
public class AddBadgeTest {
	private Map<String, String> connectionParams = null;
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _publicKey = null;
	public String _alertAppDateFormat = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		connectionParams.put(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT, "yyyyMMddHHmmss");
		_baseUrl = (String) connectionParams.get(RS2AccessItConnectorConstants.BASE_URL);
		_apiUserName = (String) connectionParams.get(RS2AccessItConnectorConstants.API_USER_NAME);
		_apiPassword = (String) connectionParams.get(RS2AccessItConnectorConstants.API_PASSWORD);
		_publicKey = (String) connectionParams.get(RS2AccessItConnectorConstants.PUBLIC_KEY);
		_alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addBadgeWithoutUserTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "UserId does not passed!!");
	}

	@Test
	public void addBadgeForWithNullUserParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "Error during addBadge for user");
	}

	@Test
	public void addBadgeForWithUserDataButNoBadgeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "Mandatory data does not passed!!");
	}

	@Test
	public void addBadgeForWithUserDataButSomeBadgeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		IProvisioningResult response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "Mandatory data does not passed!!");
	}

	@Test
	public void addBadgeForWithRandomUidUserDataAndMandatoryBadgeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		IProvisioningResult response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
	}

	@Test
	public void addBadgeForWithRandomUidWithoutWarningsFlag() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		IProvisioningResult response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
	}

	@Test
	public void addBadgeForWithUserDataAndMandatoryBadgeDataWithWrongValues() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1); // int [-1,2147483647]
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234); // int [-1,2147483647]
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false); // boolean
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(90000) + 10000); // int
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true); // boolean
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE); // boolean
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1); // int [1,2,3]
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);// int [2147483647]
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 234234);// int [32767]
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);// boolean
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);// int [32767]
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void addBadgeForValidUserAndValidBadgeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		/*
		 * observed that following fields are updated by system at the time of create
		 * badge "LastModified": "2019-12-03T17:59:49.173", "LastModifiedByUser": "rs2",
		 * "DateCreated": "2019-12-03T17:59:49.173", "CreatedByUser": "rs2",
		 * "LastCardModRowVersion": "0x0000000000076286", "DateIssued":
		 * "0001-01-01T00:00:00", "LastTimeUsed": "0001-01-01T00:00:00",
		 */
	}
	
	@Test
	public void addBadgeWithComboCode() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		// not required
		//userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		// facilty code will set as card number
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void addBadgeWithComboCodeWithFacility() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		// not required
		//userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		// facilty code is 2, random number will set as card number
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, "2-"+(new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void addBadgeWithComboCodeWithFacilityCustomSeperator() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID, "True");
		connectionParams.put(RS2AccessItConnectorConstants.FACILITYCODE_BADGEID_SEPARATOR, "$");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		// not required
		//userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		// facilty code is 2, random number will set as card number
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, "2$"+(new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void addBadgeForValidUserAndValidBadgeStatus() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 10);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void addBadgeForValidUserAndWrongUserType() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE, 13);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS, 2);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	@Test
	public void addBadgeForValidUserAndWrongAccessMode() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE, 3);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE, 8);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void addBadgeForValidUserAndWrongBadgeActiveFromDates() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE, 3);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE, "20190909121234");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE, "20190909");
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void addBadgeForValidUserAndOptionalDates() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE, 3);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE, "20190909121234");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE, "20391111121212");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_TYPE, "TEMPTEST");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_NOTES, "Description");
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void addBadgeForValidUserAndInValidBadgeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, "Alert");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, "ABCD");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		
	}

	@Test
	public void AddReAssignSamebadgeNumber() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void addSamebadgeNumberFordifferentUsersAfterDeleteFirstUser() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		userParameters.put("LastName", "Add Badge LN New");
		userParameters.put("FirstName", "Add Badge FN New");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.addTempBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void addSamebadgeNumberFordifferentUsers() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		String badgeNumber = String.valueOf(((new Random()).nextInt(900000) + 100000));
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Add Badge LN");
		userParameters.put("FirstName", "Add Badge FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, "2-"+badgeNumber);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		response = connectionInterface.activateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		response = connectionInterface.activateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_SUCCESS);
		Map<String, Object> userParametersNew = new HashMap<String, Object>();
		userParametersNew.put("LastName", "Add Badge LN New");
		userParametersNew.put("FirstName", "Add Badge FN New");
		userParametersNew.put("MemberOfAllSites", true);
		userParametersNew.put("CardholderStatus", 0);
		response = connectionInterface.create(12321L, null, userParametersNew, null, null);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 4);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, "4-"+badgeNumber);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 0);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 2);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParametersNew.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParametersNew, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.activateBadge(null, null, userParametersNew, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParametersNew, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		response = connectionInterface.activateBadge(null, null, userParametersNew, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.UNLOCK_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParametersNew, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
		response = connectionInterface.deleteAccount(2321L, null, userParametersNew, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
	}

}
