/**
 * 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.exception.RS2AccessItConnectorException;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * @author soori
 *
 */
public class TestConnectionTest {
	private Map<String, String> connectionParams = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test(expected = RS2AccessItConnectorException.class)
	public void nullConnectionParams() throws Exception {
		new RS2AccessItConnectionInterface(null);
	}

	@Test
	public void validCredentials() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.FACILITYCODE_BADGEID_SEPARATOR, "$");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		boolean testPassed = connectionInterface.testConnection();
		assertTrue(testPassed);
	}

	@Test
	public void wrongPublicKey() throws Exception {
		connectionParams.put("publicKey", "691F9674A047ACB60A14857");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void wrongCredentials() throws Exception {
		connectionParams.put("userName", "sdsd");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void wrongEndpoint() throws Exception {
		connectionParams.put("baseURL", "ssdfsdf");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	@Test
	public void emptyEndpoint() throws Exception {
		connectionParams.put("baseURL", "");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

	// TODO :: ideally this should fail , connector should have check for mandatory fields
	// too many test cases are skipped ( missing parameter , has parameter but value is null )
	@Test
	public void emptyMap() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(new HashMap<String, String>());
		boolean testConnectionResponse = connectionInterface.testConnection();
		assertFalse(testConnectionResponse);
	}

}
