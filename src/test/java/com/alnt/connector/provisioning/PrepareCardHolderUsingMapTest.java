/**
 * 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.exception.RS2AccessItConnectorException;
import com.alnt.connector.provisioning.model.CardHolder;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * @author soori
 *
 */
public class PrepareCardHolderUsingMapTest {
	private Map<String, String> connectionParams = null;
	Properties p = new Properties();
	String imageUrl = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		imageUrl = p.getProperty("userImage");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createuserWithMandatoryData() throws RS2AccessItConnectorException {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();

		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		CardHolder holder = connectionInterface.prepareCardHolder(userParameters);
		assertEquals(holder.getFirstName(), "CreateFN");
		assertNull(holder.getImages());
	}

	@Test
	public void createUserWithImageWithoutType() throws RS2AccessItConnectorException, IOException {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("ImageData",extractBytes(imageUrl));
		CardHolder holder = connectionInterface.prepareCardHolder(userParameters);
		assertFalse(holder.getImages().isEmpty());
		
	}

	@Test
	public void createUserWithImage() throws RS2AccessItConnectorException, IOException {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("ImageTypdde", 1);
		userParameters.put("ImageData", extractBytes(imageUrl));
		CardHolder holder = connectionInterface.prepareCardHolder(userParameters);
		assertFalse(holder.getImages().isEmpty());
		assertEquals(holder.getImages().get(0).getImageType().toString(), "1");
	}

	@Test
	public void createUserWithCustomFields() throws RS2AccessItConnectorException {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(new HashMap<String, String>());
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("UserText1", "Hello");
		CardHolder holder = connectionInterface.prepareCardHolder(userParameters);
		assertEquals(holder.getUserColumns().getUserText1(), "Hello");
	}
	
	private byte[] extractBytes(String ImageName) throws IOException {
		BufferedImage bImage = ImageIO.read(new File(ImageName));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bImage, "jpg", bos);
		return bos.toByteArray();
	}
	


}
