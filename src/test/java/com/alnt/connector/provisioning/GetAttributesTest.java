/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * @author soori
 *
 */
public class GetAttributesTest {
	private Map<String, String> connectionParams = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", "https://rs2tech.com:7777/v1.0/");
		connectionParams.put("userName", "rs2");
		connectionParams.put("password", "rs2");
		connectionParams.put("publicKey", "691F96B26D2B1C074A047ACB60A14857");
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		List response = connectionInterface.getAttributes();
		assertFalse(response.isEmpty());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapEmptyTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		List response = connectionInterface.getAttributes(attributesMap);
		assertTrue(response.isEmpty());
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapNullValueTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", null);
		List response = connectionInterface.getAttributes(attributesMap);
		assertTrue(response.isEmpty());
	}
	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapEMptyValueTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", "");
		List response = connectionInterface.getAttributes(attributesMap);
		assertTrue(response.isEmpty());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapInvalidAttributeTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", "rs2");
		List response = connectionInterface.getAttributes(attributesMap);
		assertTrue(response.isEmpty());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapBadgeStatusModeTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", RS2AccessItConnectorConstants.ATTR_BADGE_STATUS);
		List response = connectionInterface.getAttributes(attributesMap);
		assertFalse(response.isEmpty());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapUserStatusTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", RS2AccessItConnectorConstants.ATTR_USER_STATTUS);
		List response = connectionInterface.getAttributes(attributesMap);
		assertFalse(response.isEmpty());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapLocksetAccessModeTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE);
		List response = connectionInterface.getAttributes(attributesMap);
		assertFalse(response.isEmpty());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapLocksetUserTypeTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE);
		List response = connectionInterface.getAttributes(attributesMap);
		assertFalse(response.isEmpty());
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void getAttributesMapImageTypeTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, String> attributesMap = new HashMap<String, String>();
		attributesMap.put("API_NAME", RS2AccessItConnectorConstants.ATTR_USER_IMAGETYPE);
		List response = connectionInterface.getAttributes(attributesMap);
		assertFalse(response.isEmpty());
	}

}
