package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.helper.RS2AccessItClientHelper;
import com.alnt.connector.provisioning.helper.RS2AccessItURLConnection;
import com.alnt.connector.provisioning.model.AccessLevel;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author soori
 *
 */
public class ChangeBadgeRolesTest {
	private Map<String, String> connectionParams = null;
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _publicKey = null;
	public String _alertAppDateFormat = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		connectionParams.put(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT, "yyyyMMddHHmmss");
		_baseUrl = (String) connectionParams.get(RS2AccessItConnectorConstants.BASE_URL);
		_apiUserName = (String) connectionParams.get(RS2AccessItConnectorConstants.API_USER_NAME);
		_apiPassword = (String) connectionParams.get(RS2AccessItConnectorConstants.API_PASSWORD);
		_publicKey = (String) connectionParams.get(RS2AccessItConnectorConstants.PUBLIC_KEY);
		_alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
	}

	@After
	public void tearDown() throws Exception {
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeBadgeRolesWithNullUserParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeBadgeRoles(null, provisionRoles, userParameters, null,
				null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "Error during changeBadgeRoles");

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeBadgeRolesWithOutUserParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeBadgeRoles(null, provisionRoles, userParameters, null,
				null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "UserId does not passed!!");
	}

	@Test
	public void changeBadgeForWithUserDataButNoBadgeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.changeBadgeRoles(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "badgeId does not passed!!");
	}

	@Test
	public void changeBadgeInvalidUserAndInvalidCardNumber() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		IProvisioningResult response = connectionInterface.changeBadgeRoles(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "badge does not exist in external system");
	}

	@Test
	public void changeBadgeInvalidUserAndInvalidCardNumberAndSkipErrors() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		IProvisioningResult response = connectionInterface.changeBadgeRoles(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
	}

	@Test
	public void changeBadgeRolesForUserHasRoles() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Badge Roles LN");
		userParameters.put("FirstName", "Change Badge Roles FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191212090909",
				"20291212233445");
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191201090909", "20191212090909");
		int i = 0;
		for (IRoleInformation role : roles) {
			if (i <= 1) {
				role.setRoleAction(IProvisoiningConstants.ADD_ROLE);
			} else {
				role.setRoleAction(null);
			}
			i++;
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		i = 0;
		for (IRoleInformation role : roles) {
			if (i < 1) {
				role.setRoleAction(IProvisoiningConstants.REMOVE_ROLE);
			} else {
				role.setRoleAction(null);
			}
			i++;
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		response = connectionInterface.activateBadge(null, null, userParameters, null, null);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}
	
	
	@Test
	public void changeBadgeRolesForUserHasRolesWithCombinedCode() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Badge Roles LN");
		userParameters.put("FirstName", "Change Badge Roles FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, "2-"+(new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191212090909",
				"20291212233445");
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE, "20190104121212");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE, "20290104111111");
		response = connectionInterface.changeBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191201090909", "20191212090909");
		int i = 0;
		for (IRoleInformation role : roles) {
			if (i <= 1) {
				role.setRoleAction(IProvisoiningConstants.ADD_ROLE);
			} else {
				role.setRoleAction(null);
			}
			i++;
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		i = 0;
		for (IRoleInformation role : roles) {
			if (i < 1) {
				role.setRoleAction(IProvisoiningConstants.REMOVE_ROLE);
			} else {
				role.setRoleAction(null);
			}
			i++;
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		response = connectionInterface.activateBadge(null, null, userParameters, null, null);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}


	@Test
	public void changeBadgeRolesWithValidToForUserWithoutRoles() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Badge Roles LN");
		userParameters.put("FirstName", "Change Badge Roles FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), null,
				"20191212090909");
		for (IRoleInformation role : roles) {
			role.setRoleAction(IProvisoiningConstants.ADD_ROLE);
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		for (IRoleInformation role : roles) {
			role.setRoleAction(IProvisoiningConstants.REMOVE_ROLE);
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		response = connectionInterface.activateBadge(null, null, userParameters, null, null);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}

	@Test
	public void changeBadgeRolesWithValidFromForUserWithoutRoles() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Badge Roles LN");
		userParameters.put("FirstName", "Change Badge Roles FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191212090909",
				null);
		for (IRoleInformation role : roles) {
			role.setRoleAction(IProvisoiningConstants.ADD_ROLE);
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		for (IRoleInformation role : roles) {
			role.setRoleAction(IProvisoiningConstants.REMOVE_ROLE);
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		response = connectionInterface.activateBadge(null, null, userParameters, null, null);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}

	// TODO :: Get confirmation from API
	// These roles are not visible in UI, above test cases are visible in UI
	@Test
	public void changeBadgeRolesWithBothDatesForUserWithoutRoles() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Badge Roles LN");
		userParameters.put("FirstName", "Change Badge Roles FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191212090909",
				"20191212090909");
		for (IRoleInformation role : roles) {
			role.setRoleAction(IProvisoiningConstants.ADD_ROLE);
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		for (IRoleInformation role : roles) {
			role.setRoleAction(IProvisoiningConstants.REMOVE_ROLE);
		}
		response = connectionInterface.changeBadgeRoles(null, roles, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		response = connectionInterface.activateBadge(null, null, userParameters, null, null);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}

	private IRoleInformation prepareRoleInformation(String name, String desc, String ValidFrom, String ValidTo) {
		String _alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(_alertAppDateFormat);
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(desc);
		try {
			if (null != ValidFrom && !ValidFrom.isEmpty()) {
				roleInformation.setValidFrom(formatter.parse(ValidFrom));
			}
			if (null != ValidTo && !ValidTo.isEmpty()) {
				roleInformation.setValidTo(formatter.parse(ValidTo));
			}
		} catch (Exception _ex) {
		}
		return roleInformation;
	}

	private List<AccessLevel> getAccessRolesFromSystem() throws  IOException, JSONException , Exception {
		String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_ACCESS_LEVELS);
		RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint,
				RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
		if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
			String output = bufferedReader.readLine();
			if (output != null) {
				JSONArray restapiresponseArray = new JSONArray(output);
				if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
					Type listType = new TypeToken<List<AccessLevel>>() {
					}.getType();
					return new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
				}
			}
		}
		return null;
	}

	private List<IRoleInformation> convertAccessLevelsToRoleInfo(List<AccessLevel> accessLevels, String startDate,
			String endDate) {
		List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
		for (AccessLevel accessLevel : accessLevels) {
			String roleId = accessLevel.getAccessLevelID();
			String roleName = accessLevel.getAccessLevelName();
			String roleDesc = accessLevel.getAccessLevelName();
			Map<String, List<ExtractorAttributes>> options = new HashMap<>();
			IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc, startDate, endDate);
			Map<String, List<String>> memberData = new HashMap<String, List<String>>();
			List<String> values = new ArrayList<String>();
			values.add("" + roleId);
			memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
			getRoleAttribute(options, "", "", memberData);
			roleInformation.setMemberData(memberData);
			rolesList.add(roleInformation);
		}
		return rolesList;
	}

	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleId,
			String roleDesc, String ValidFrom, String ValidTo) {
		String _alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(_alertAppDateFormat);
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleDesc);
		roleInformation.setDescription(roleDesc);
		try {
			if (null != ValidFrom && !ValidFrom.isEmpty()) {
				roleInformation.setValidFrom(formatter.parse(ValidFrom));
			}
			if (null != ValidTo && !ValidTo.isEmpty()) {
				roleInformation.setValidTo(formatter.parse(ValidTo));
			}
		} catch (Exception _ex) {
		}
		return roleInformation;
	}

	private void getRoleAttribute(Map<String, List<ExtractorAttributes>> options, String attributeName,
			String attributeValue, Map<String, List<String>> memberData) {
		if (options.containsKey(attributeName)) {
			List<String> values = new ArrayList<String>();
			List<ExtractorAttributes> companyAttr = options.get(attributeName);
			for (ExtractorAttributes extractorAttributes : companyAttr) {
				if (extractorAttributes.isRoleAttr()) {
					values = new ArrayList<String>();
					values.add("" + attributeValue);
					memberData.put(extractorAttributes.getAttributeName(), values);
				}
			}
		}
	}

}
