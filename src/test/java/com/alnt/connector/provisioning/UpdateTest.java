package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * 
 * @author soori
 *
 */
public class UpdateTest {
	private Map<String, String> connectionParams = null;
	Properties p = new Properties();
	String imageUrl = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		imageUrl = p.getProperty("userImage");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void updateUserWithoutCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}
	
	@Test
	public void updateUserWitNullParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}

	@Test
	public void updateUserRandomCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_FAILURE);
	}
	@Test
	public void updateUserRandomCardHolderIdAndIgnoreErrorsParam() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
	}

	@Test
	public void updateUserUserProvisioned() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "updateLN");
		userParameters.put("FirstName", "updateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put("LastName", "updateLN Upated");
		userParameters.put("FirstName", "updateFN Upated");
		response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void updateUserUserWithImageProvisioned() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "updateLN");
		userParameters.put("FirstName", "updateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put("LastName", "updateLN Upated");
		userParameters.put("FirstName", "updateFN Upated");
		userParameters.put("ImageData", extractBytes(imageUrl));
		response = connectionInterface.update(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	private byte[] extractBytes(String ImageName) throws IOException {
		BufferedImage bImage = ImageIO.read(new File(ImageName));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bImage, "jpg", bos);
		return bos.toByteArray();
	}

	

}
