package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * 
 * @author soori
 *
 */
public class LockTest {
	private Map<String, String> connectionParams = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void lockUserWithoutCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
	}
	
	@Test
	public void lockUserWitNullParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
	}

	@Test
	public void lockUserRandomCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
	}

	@Test
	public void lockUserUserProvisioned() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "LockLN");
		userParameters.put("FirstName", "LockFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		assertTrue(connectionInterface.isUserLocked(userParameters.get(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID).toString()));
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void lockAlreadyDeletedUserConnParamShowWarningsTrue() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "LockLN");
		userParameters.put("FirstName", "LockFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void lockAlreadyLockedUserConnParamShowWarningsTrue() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "LockLN");
		userParameters.put("FirstName", "LockFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 0);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void lockAlreadyDeletedUserwithoutConnParamDefaultTrue() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "LockLN");
		userParameters.put("FirstName", "LockFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void lockAlreadyDeletedUserConnParamShowWarningsFalse() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "LockLN");
		userParameters.put("FirstName", "LockFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		response = connectionInterface.lock(2321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

}
