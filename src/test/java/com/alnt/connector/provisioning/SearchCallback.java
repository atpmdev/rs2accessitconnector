/**
 * soori
 * 2019-11-25 
 */
package com.alnt.connector.provisioning;

import java.util.List;

import com.alnt.extractionconnector.common.model.ReconExtensionInfo;
import com.alnt.extractionconnector.common.service.ISearchCallback;

/**
 * @author soori
 *
 */
public class SearchCallback implements ISearchCallback {

	/**
	 * soori - 7:14:11 pm
	 * @param arg0
	 * @throws Exception
	 * @see com.alnt.extractionconnector.common.service.ISearchCallback#processSearchResult(java.util.List)
	 */
	@SuppressWarnings("rawtypes")
	public void processSearchResult(List arg0) throws Exception {
	}

	/**
	 * soori - 7:14:11 pm
	 * @param arg0
	 * @param arg1
	 * @throws Exception
	 * @see com.alnt.extractionconnector.common.service.ISearchCallback#processSearchResult(java.util.List, com.alnt.extractionconnector.common.model.ReconExtensionInfo)
	 */
	@SuppressWarnings("rawtypes")
	public void processSearchResult(List arg0, ReconExtensionInfo arg1) throws Exception {
		

	}

}
