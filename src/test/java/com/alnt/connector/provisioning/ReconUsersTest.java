/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertTrue;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;

/**
 * @author soori
 *
 */
public class ReconUsersTest {
	private Map<String, String> connectionParams = null;
	Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		connectionParams.put(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT, "yyyyMMddHHmmss");
		
		
		List<ExtractorAttributes> attributes = new ArrayList<ExtractorAttributes>();
		ExtractorAttributes userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_ID, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_ID, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_FN, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_FN, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_LN, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_LN, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_MEMEBER_OF_ALL_SITES, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_MEMEBER_OF_ALL_SITES, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA, "",
				IExtractionConstants.TYPE.IMAGE);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_STATTUS, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_STATTUS, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_COMPANY_ID, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_COMPANY_ID, attributes);

		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_MIDDLE_INITIAL, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setUserAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_MIDDLE_INITIAL, attributes);

		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, attributes);

		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, attributes);
		// date has  milliseconds
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_DATE, "",
				IExtractionConstants.TYPE.DATE);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_DATE, attributes);
		// date has no milliseconds
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_DATE_ISSUED, "",
				IExtractionConstants.TYPE.DATE);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_DATE_ISSUED, attributes);
		
		
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, attributes);

		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, attributes);

		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setBadgeAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_ROLE_ID, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_ROLE_ID, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_ROLE_ACTIVE_DATE, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_ROLE_ACTIVE_DATE, attributes);
		attributes = new ArrayList<ExtractorAttributes>();
		userAttribute = new ExtractorAttributes(RS2AccessItConnectorConstants.ATTR_USER_ROLE_EXPITY_DATE, "",
				IExtractionConstants.TYPE.STRING);
		userAttribute.setRoleAttr(true);
		attributes.add(userAttribute);
		options.put(RS2AccessItConnectorConstants.ATTR_USER_ROLE_EXPITY_DATE, attributes);
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void userReconTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getAllUsersWithCallback(options, 0, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userReconTestToCombineFaciltyCodeandBadgeId() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getAllUsersWithCallback(options, 0, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userIncrementalReconTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getIncrementalUsersWithCallback(new Date(), options, 0, null, callback);
		assertTrue(true);
	}
	
	@Test
	public void userIncrementalMonthReconTest() throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.MONTH, -1);
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		connectionInterface.getIncrementalUsersWithCallback(cal.getTime(), options, 0, null, callback);
		assertTrue(true);
	}

	@Test
	public void userReconTestInvalidEndpoint() throws Exception {
		connectionParams.put("baseURL", "https://rs2tech.com:7777/v2.0/");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISearchCallback callback = new SearchCallback();
		Map<String, List<ExtractorAttributes>> options = new HashMap<String, List<ExtractorAttributes>>();
		connectionInterface.getAllUsersWithCallback(options, 0, null, callback);
		assertTrue(true);

	}

}
