package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * 
 * @author soori
 *
 */
public class DeactivateBadgeTest {
	private Map<String, String> connectionParams = null;
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _publicKey = null;
	public String _alertAppDateFormat = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		connectionParams.put(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT, "yyyyMMddHHmmss");
		_baseUrl = (String) connectionParams.get(RS2AccessItConnectorConstants.BASE_URL);
		_apiUserName = (String) connectionParams.get(RS2AccessItConnectorConstants.API_USER_NAME);
		_apiPassword = (String) connectionParams.get(RS2AccessItConnectorConstants.API_PASSWORD);
		_publicKey = (String) connectionParams.get(RS2AccessItConnectorConstants.PUBLIC_KEY);
		_alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void deActivateBadgeWithoutUserTest() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "UserId does not passed!!");
	}

	@Test
	public void deActivateBadgeForWithNullUserParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "Error during deactivating a user badge");
	}

	@Test
	public void deActivateBadgeForWithUserDataButNoBadgeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "badgeId does not passed!!");
	}
	
	@Test
	public void deActivateBadgeInvalidUserAndInvalidCardNumber() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		IProvisioningResult response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "badge does not exist in external system");
	}
	
	@Test
	public void deActivateBadgeInvalidUserAndInvalidCardNumberAndSkipErrors() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		IProvisioningResult response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
	}


	

	@Test
	public void deActivateInactiveWarningsIgnoreFlag() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Deactivate Badge LN");
		userParameters.put("FirstName", "Deactivate Badge LN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 0);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}
	
	@Test
	public void deActivateInActiveBadge() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Deactivate Badge LN");
		userParameters.put("FirstName", "Deactivate Badge LN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 0);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_FAILURE);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}
	
	
	@Test
	public void deActivateActiveBadge() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Deactivate Badge LN");
		userParameters.put("FirstName", "Deactivate Badge LN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, 1234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, false);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, (new Random()).nextInt(900000) + 100000);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, Boolean.TRUE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, 1);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, 234234);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, 32);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, Boolean.FALSE);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, 10);
		response = connectionInterface.addBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		response = connectionInterface.deActivateBadge(null, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.LOCK_USER_SUCCESS);
		response = connectionInterface.removeBadge(null, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(null, null, userParameters, null, null);
	}

}
