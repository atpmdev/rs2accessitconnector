package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import org.apache.commons.httpclient.HttpStatus;
import org.json.JSONArray;
import org.json.JSONException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.helper.RS2AccessItClientHelper;
import com.alnt.connector.provisioning.helper.RS2AccessItURLConnection;
import com.alnt.connector.provisioning.model.AccessLevel;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author soori
 *
 */
public class ChangeAccessTest {
	private Map<String, String> connectionParams = null;
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _publicKey = null;
	public String _alertAppDateFormat = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		connectionParams.put(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT, "yyyyMMddHHmmss");
		_baseUrl = (String) connectionParams.get(RS2AccessItConnectorConstants.BASE_URL);
		_apiUserName = (String) connectionParams.get(RS2AccessItConnectorConstants.API_USER_NAME);
		_apiPassword = (String) connectionParams.get(RS2AccessItConnectorConstants.API_PASSWORD);
		_publicKey = (String) connectionParams.get(RS2AccessItConnectorConstants.PUBLIC_KEY);
		_alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * if both provisioning roles & deprovisioning roles are null/ empty should it
	 * return success or failure.
	 */

	@Test
	public void changeAccessWithoutRolesToChange() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.changeAccess(null, null, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithOutUserParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "UserId does not passed!!");
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithNullUserParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters =null;
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserId() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void changeAccessForWithRandomUserIdbutNoWarningsFlag() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		List provisionRoles = new ArrayList();
		provisionRoles.add(prepareRoleInformation("role1", "desc", null, null));
		IProvisioningResult response = connectionInterface.changeAccess(null, provisionRoles, userParameters, null,
				null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
	}

	@Test
	public void changeAccessAddValidRole() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), null, null);
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	
	@Test
	public void changeAccessAddValidRoleWithActiveDate() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191212090909",
				null);
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddValidRoleWithDeActiveDate() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), null,
				"20291212090909");
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddValidRoleWithBothDates() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191212090909",
				"20291212233445");
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessDeleteValidRoleWithBothDates() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = convertAccessLevelsToRoleInfo(getAccessRolesFromSystem(), "20191212090909",
				"20291212233445");
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_SUCCESS);
		List<IRoleInformation> deProvisionRoles = new ArrayList<IRoleInformation>();
		Random rand = new Random();
		deProvisionRoles.add(roles.get(rand.nextInt(roles.size())));
		response = connectionInterface.changeAccess(null, null, userParameters, null, deProvisionRoles, null);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddInValidRoleWithBothDates() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = new ArrayList<IRoleInformation>();
		roles.add(prepareRoleInformation("ROLE1", "ROLEDSC", "20191212090909", "20291212233445"));
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void changeAccessAddInValidRoleIdWithBothDates() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "Change Access LN");
		userParameters.put("FirstName", "Change Access FN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		List<IRoleInformation> roles = new ArrayList<IRoleInformation>();
		roles.add(prepareRoleInformationWithInValidRoleId("ROLE1", "ROLEDSC", "20191212090909", "20291212233445"));
		response = connectionInterface.changeAccess(null, roles, userParameters, null, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.ASSIGN_ROLES_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	private IRoleInformation prepareRoleInformation(String name, String desc, String ValidFrom, String ValidTo) {

		String _alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(_alertAppDateFormat);
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(desc);
		try {
			if (null != ValidFrom && !ValidFrom.isEmpty()) {
				roleInformation.setValidFrom(formatter.parse(ValidFrom));
			}
			if (null != ValidTo && !ValidTo.isEmpty()) {
				roleInformation.setValidTo(formatter.parse(ValidTo));
			}
		} catch (Exception _ex) {

		}
		return roleInformation;
	}

	private IRoleInformation prepareRoleInformationWithInValidRoleId(String name, String desc, String ValidFrom,
			String ValidTo) {

		String _alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(_alertAppDateFormat);
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(name);
		roleInformation.setDescription(desc);
		try {
			if (null != ValidFrom && !ValidFrom.isEmpty()) {
				roleInformation.setValidFrom(formatter.parse(ValidFrom));
			}
			if (null != ValidTo && !ValidTo.isEmpty()) {
				roleInformation.setValidTo(formatter.parse(ValidTo));
			}
		} catch (Exception _ex) {

		}
		Map<String, List<String>> memberData = new HashMap<String, List<String>>();
		List<String> values = new ArrayList<String>();
		values.add("" + UUID.randomUUID().toString());
		memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
		Map<String, List<ExtractorAttributes>> options = new HashMap<>();
		getRoleAttribute(options, "", "", memberData);
		roleInformation.setMemberData(memberData);
		return roleInformation;
	}

	private List<AccessLevel> getAccessRolesFromSystem() throws IOException, JSONException,Exception {
		String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_ACCESS_LEVELS);
		RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint,
				RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
		if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
			String output = bufferedReader.readLine();
			if (output != null) {
				JSONArray restapiresponseArray = new JSONArray(output);
				if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
					Type listType = new TypeToken<List<AccessLevel>>() {
					}.getType();
					return new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
				}
			}
		}
		return null;
	}

	private List<IRoleInformation> convertAccessLevelsToRoleInfo(List<AccessLevel> accessLevels, String startDate,
			String endDate) {
		List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
		for (AccessLevel accessLevel : accessLevels) {
			String roleId = accessLevel.getAccessLevelID();
			String roleName = accessLevel.getAccessLevelName();
			String roleDesc = accessLevel.getAccessLevelName();
			Map<String, List<ExtractorAttributes>> options = new HashMap<>();
			IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc, startDate, endDate);
			Map<String, List<String>> memberData = new HashMap<String, List<String>>();
			List<String> values = new ArrayList<String>();
			values.add("" + roleId);
			memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
			getRoleAttribute(options, "", "", memberData);
			roleInformation.setMemberData(memberData);
			rolesList.add(roleInformation);
		}
		return rolesList;
	}

	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleId,
			String roleDesc, String ValidFrom, String ValidTo) {
		String _alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
		SimpleDateFormat formatter = new SimpleDateFormat(_alertAppDateFormat);
		IRoleInformation roleInformation = new RoleInformation();
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleDesc);
		roleInformation.setDescription(roleDesc);
		try {
			if (null != ValidFrom && !ValidFrom.isEmpty()) {
				roleInformation.setValidFrom(formatter.parse(ValidFrom));
			}
			if (null != ValidTo && !ValidTo.isEmpty()) {
				roleInformation.setValidTo(formatter.parse(ValidTo));
			}
		} catch (Exception _ex) {
		}
		return roleInformation;
	}

	private void getRoleAttribute(Map<String, List<ExtractorAttributes>> options, String attributeName,
			String attributeValue, Map<String, List<String>> memberData) {
		if (options.containsKey(attributeName)) {
			List<String> values = new ArrayList<String>();
			List<ExtractorAttributes> companyAttr = options.get(attributeName);
			for (ExtractorAttributes extractorAttributes : companyAttr) {
				if (extractorAttributes.isRoleAttr()) {
					values = new ArrayList<String>();
					values.add("" + attributeValue);
					memberData.put(extractorAttributes.getAttributeName(), values);
				}
			}
		}
	}

}
