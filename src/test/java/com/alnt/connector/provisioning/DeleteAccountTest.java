/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * @author soori
 *
 */
public class DeleteAccountTest {
	private Map<String, String> connectionParams = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void deleteWithoutCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	    assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}
	
	@Test
	public void deleteWithoutCardHolderIdNullParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = null;
		IProvisioningResult response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	    assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}
	
	
	@Test
	public void deleteRandomCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	    assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}
	
	@Test
	public void deleteUserProvisioned() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "DeleteLN");
		userParameters.put("FirstName", "DeleteFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	    assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
	}
	
	
	
	@Test
	public void deleteAlreadyDeletedUserConnParamShowWarningsTrue() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "True");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "DeleteLN");
		userParameters.put("FirstName", "DeleteFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	    assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}
	
	@Test
	public void deleteAlreadyDeletedUserwithoutConnParamDefaultTrue() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "DeleteLN");
		userParameters.put("FirstName", "DeleteFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	    assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_FAILURE);
	}
	
	@Test
	public void deleteAlreadyDeletedUserConnParamShowWarningsFalse() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "DeleteLN");
		userParameters.put("FirstName", "DeleteFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
		response = connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	    assertEquals(response.getMsgCode(), IProvisoiningConstants.DELETE_USER_SUCCESS);
	}

}
