/**
 * soori
 * 2019-11-21 
 */
package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * @author soori
 *
 */
public class CreateTest {
	private Map<String, String> connectionParams = null;
	Properties p = new Properties();
	String imageUrl = null;

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		imageUrl = p.getProperty("userImage");
	}

	/**
	 * soori - 11:38:10 pm
	 * 
	 * void
	 * 
	 * @throws java.lang.Exception
	 *
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void createUserWithMandatoryData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageAndStringCustomData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("Department", "Dept");
		userParameters.put("UserText1", "UserText1");
		userParameters.put("UserText2", "UserText2");
		userParameters.put("UserText3", "UserText3");
		userParameters.put("UserText4", "UserText4");
		userParameters.put("UserText5", "UserText5");
		userParameters.put("UserText6", "UserText6");
		userParameters.put("UserText7", "UserText7");
		userParameters.put("UserText8", "UserText8");
		userParameters.put("UserText9", "UserText9");
		userParameters.put("UserText10", "UserText10");
		userParameters.put("UserText11", "UserText11");
		userParameters.put("UserText12", "UserText12");
		userParameters.put("UserText13", "UserText13");
		userParameters.put("UserText14", "UserText14");
		userParameters.put("UserText15", "UserText15");
		userParameters.put("UserText16", "UserText16");
		userParameters.put("UserText17", "UserText17");
		userParameters.put("UserText18", "UserText18");
		userParameters.put("UserText19", "UserText19");
		userParameters.put("UserText20", "UserText20");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageAndNumericCustomData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("Department", "Dept");
		userParameters.put("UserNumeric155", "1");
		userParameters.put("UserNumeric2", "2");
		userParameters.put("UserNumeric3", 3);
		userParameters.put("UserNumeric4", 4);
		userParameters.put("UserNumeric5", 5);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageAndInvalidNumericCustomData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("Department", "Dept");
		userParameters.put("UserNumeric1", "1");
		userParameters.put("UserNumeric2", "A");
		userParameters.put("UserNumeric3", 3);
		userParameters.put("UserNumeric4", 4);
		userParameters.put("UserNumeric5", 5);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageAndDateCustomData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("ImageType", 1);
		userParameters.put("ImageData", extractBytes(imageUrl));
		userParameters.put("Department", "Dept");
		userParameters.put("UserText1", "UserText1");
		userParameters.put("UserText2", "UserText2");
		userParameters.put("UserText3", "UserText3");
		userParameters.put("UserText4", "UserText4");
		userParameters.put("UserText5", "UserText5");
		userParameters.put("UserText6", "UserText6");
		userParameters.put("UserText7", "UserText7");
		userParameters.put("UserText8", "UserText8");
		userParameters.put("UserText9", "UserText9");
		userParameters.put("UserText10", "UserText10");
		userParameters.put("UserText11", "UserText11");
		userParameters.put("UserText12", "UserText12");
		userParameters.put("UserText13", "UserText13");
		userParameters.put("UserText14", "UserText14");
		userParameters.put("UserText15", "UserText15");
		userParameters.put("UserText16", "UserText16");
		userParameters.put("UserText17", "UserText17");
		userParameters.put("UserText18", "UserText18");
		userParameters.put("UserText19", "UserText19");
		userParameters.put("UserText20", "UserText20");
		userParameters.put("UserNumeric1", "1");
		userParameters.put("UserNumeric2", "2");
		userParameters.put("UserNumeric3", 3);
		userParameters.put("UserNumeric4", 4);
		userParameters.put("UserNumeric5", 5);
		userParameters.put("UserDate1", "2019-02-03");
		userParameters.put("UserDate2", "2019-12-25T00:00:00");
		userParameters.put("UserDate3", "2050-12-23T21:50:24");
		userParameters.put("UserDate4", "2023-11-01T18:49:41");
		userParameters.put("UserDate5", "2014-02-09T01:13:34");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageAndFullCustomData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("Department", "Dept");
		userParameters.put("UserDate1", "2019-02-03");
		userParameters.put("UserDate2", "2019-12-25T00:00:00");
		userParameters.put("UserDate3", "2050-12-23T21:50:24");
		userParameters.put("UserDate4", "2023-11-01T18:49:41");
		userParameters.put("UserDate5", "2014-02-09T01:13:34");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageAndInvalidDateCustomData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("Department", "Dept");
		userParameters.put("UserDate1", "2019/02/03");
		userParameters.put("UserDate2", "20191225T00:00:00");
		userParameters.put("UserDate3", "2050-12-23T21:50:24");
		userParameters.put("UserDate4", "2023-11-01T18:49:41");
		userParameters.put("UserDate5", "2014-02-09T01:13:34");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("ImageType", 1);
		userParameters.put("ImageData", extractBytes(imageUrl));
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithImageAndWithoutImageTypeData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("ImageData",extractBytes(imageUrl));
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithInvalidData() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "CreateLN");
		userParameters.put("FirstName", "CreateFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		userParameters.put("MiddleInitial", "this field should have max length 5");
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void checkIsUserProvisioned() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "checkIsUserProvisionedLN");
		userParameters.put("FirstName", "checkIsUserProvisionedFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult createresponse = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(createresponse.getMsgCode(), IProvisoiningConstants.CREATE_USER_SUCCESS);
		ISystemInformation isUserProvisionedResponse = connectionInterface
				.isUserProvisioned(userParameters.get(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID).toString());
		assertTrue(isUserProvisionedResponse.isProvisioned());
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

	@Test
	public void createUserWithInvalidConnection() throws Exception {
		connectionParams.put("baseURL", "https://rs2tech.com:77775/v1.0/");
		connectionParams.put("userName", "rs25");
		connectionParams.put("password", "r5s2");
		connectionParams.put("publicKey", "691F96B26D2B51C074A047ACB60A14857");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "checkIsUserProvisionedLN");
		userParameters.put("FirstName", "checkIsUserProvisionedFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult createresponse = connectionInterface.create(12321L, null, userParameters, null, null);
		assertEquals(createresponse.getMsgCode(), IProvisoiningConstants.CREATE_USER_FAILURE);
	}

	@Test
	public void checkIsUserProvisionedRandonUUID() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISystemInformation isUserProvisionedResponse = connectionInterface
				.isUserProvisioned(UUID.randomUUID().toString());
		assertFalse(isUserProvisionedResponse.isProvisioned());
	}

	@Test
	public void checkIsUserProvisionedInvalidUUID() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISystemInformation isUserProvisionedResponse = connectionInterface
				.isUserProvisioned(UUID.randomUUID().toString().concat("-Extra"));
		assertFalse(isUserProvisionedResponse.isProvisioned());
	}

	@Test
	public void checkIsUserProvisionedInvalidConnection() throws Exception {
		connectionParams.put("baseURL", "https://rs2tech.com:77775/v1.0/");
		connectionParams.put("userName", "rs25");
		connectionParams.put("password", "r5s2");
		connectionParams.put("publicKey", "691F96B26D2B51C074A047ACB60A14857");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISystemInformation isUserProvisionedResponse = connectionInterface
				.isUserProvisioned(UUID.randomUUID().toString());
		assertFalse(isUserProvisionedResponse.isProvisioned());
	}

	@Test
	@Ignore
	public void validateValidUserIdFromSystem() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		ISystemInformation isUserProvisionedResponse = connectionInterface
				.isUserProvisioned("7365bd73-14c8-4965-90f1-78192de6a273");
		assertFalse(!isUserProvisionedResponse.isProvisioned());
	}
	
	private byte[] extractBytes(String ImageName) throws IOException {
		BufferedImage bImage = ImageIO.read(new File(ImageName));
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ImageIO.write(bImage, "jpg", bos);
		return bos.toByteArray();
	}

}
