package com.alnt.connector.provisioning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface;

/**
 * 
 * @author soori
 *
 */
public class DelimitTest {
	private Map<String, String> connectionParams = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * 
	 * soori 2019-11-27 - 4:30:49 pm void
	 * 
	 * @throws Exception
	 *
	 */

	@Before
	public void setUp() throws Exception {
		Properties p = new Properties();
		p.load(new FileReader("src/test/resources/testdata.properties"));
		connectionParams = new HashMap<String, String>();
		connectionParams.put("baseURL", p.getProperty("baseURL"));
		connectionParams.put("userName", p.getProperty("userName"));
		connectionParams.put("password", p.getProperty("password"));
		connectionParams.put("publicKey", p.getProperty("publicKey"));
		connectionParams.put(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT,"yyyyMMddHHmmss" );
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void delimitUserWithoutCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_DELIMIT_USER, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELIMIT_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "userId does not passed!!");
		
	}

	@Test
	public void changeDatesUserWithoutCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE);
		assertEquals(response.getMsgDesc(), "userId does not passed!!");
	}

	@Test
	public void delimitUserWithoutValidTo() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_DELIMIT_USER, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELIMIT_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "user expirydate does not passed!!");
	}

	@Test
	public void changeDatesWithoutValidTo() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE);
		assertEquals(response.getMsgDesc(), "user expirydate does not passed!!");
	}

	@Test
	public void changeDatesWithoutValidFrom() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127193400");
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE);
		assertEquals(response.getMsgDesc(), "user startdate does not passed for change validity dates action!!");
	}

	@Test
	public void delimitUserRandomCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127193400");
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_DELIMIT_USER, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELIMIT_USER_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
		
	}

	@Test
	public void changeDatesUserRandomCardHolderIdParam() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127193400");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE, "20991127193400");
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE);
		assertEquals(response.getMsgDesc(), "User does not exist in external system");
	}
	@Test
	public void delimitUserRandomCardHolderIdParamWithoutWarnings() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127193400");
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_DELIMIT_USER, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELIMIT_USER_SUCCESS);
		assertNull(response.getMsgDesc());
	}

	@Test
	public void changeDatesUserRandomCardHolderIdParamWithoutWarnings() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put(RS2AccessItConnectorConstants.RS2ACCESSIT_USERID, UUID.randomUUID().toString());
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127193400");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE, "20991127193400");
		IProvisioningResult response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_VALIDITY_DATES_SUCCESS);
		assertNull(response.getMsgDesc());
	}
	
	
	@Test
	public void delimitValidUser() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "delimitUserLN");
		userParameters.put("FirstName", "delimitUserFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127193400");
		response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_DELIMIT_USER, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELIMIT_USER_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void changeValidDatesUser() throws Exception {
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "delimitUserLN");
		userParameters.put("FirstName", "delimitUserFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20391127193400");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE, "20191127193400");
		response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_VALIDITY_DATES_SUCCESS);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void changeUserInvalidDateFormat() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "delimitUserLN");
		userParameters.put("FirstName", "delimitUserFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127193400");
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE, "20191127");
		response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}
	
	@Test
	public void delimitUserInvalidDateFormat() throws Exception {
		connectionParams.put(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS, "False");
		RS2AccessItConnectionInterface connectionInterface = new RS2AccessItConnectionInterface(connectionParams);
		Map<String, Object> userParameters = new HashMap<String, Object>();
		userParameters.put("LastName", "delimitUserLN");
		userParameters.put("FirstName", "delimitUserFN");
		userParameters.put("MemberOfAllSites", true);
		userParameters.put("CardholderStatus", 1);
		IProvisioningResult response = connectionInterface.create(12321L, null, userParameters, null, null);
		userParameters.put(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, "20191127");
		response = connectionInterface.delimitUser(2321L, null, userParameters, null,
				IProvisoiningConstants.PROV_ACTION_DELIMIT_USER, null);
		assertEquals(response.getMsgCode(), IProvisoiningConstants.DELIMIT_USER_FAILURE);
		connectionInterface.deleteAccount(2321L, null, userParameters, null, null);
	}

}
