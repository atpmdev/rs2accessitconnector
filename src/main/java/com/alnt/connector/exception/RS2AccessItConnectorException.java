package com.alnt.connector.exception;

public class RS2AccessItConnectorException extends Exception{

	private static final long serialVersionUID = 410782776743392490L;

	public RS2AccessItConnectorException(String message) {
		super(message);
	}
}
