package com.alnt.connector.constants;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RS2AccessItConnectorConstants {

	public static final String CONNECTOR_NAME = "RS2AccessItConnector";
	public static final String RS2ACCESSIT_USERID = "CardholderId";
	public static final String AE_USERID = "UserId";

	// CONNECTOR SPECIFIC
	public static final String API_USER_NAME = "userName";
	public static final String API_PASSWORD = "password";
	public static final String PUBLIC_KEY = "publicKey";
	public static final String BASE_URL = "baseURL";
	public static final String ALERT_APP_DATE_FORMAT = "alertAppDateFormat";
	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	public static final String DEFAULT_DATE_FORMAT_SECONDS = "yyyy-MM-dd'T'HH:mm:ss.SSS";
	public static final String ENABLE_PROVISION_WARNINGS = "enableProvisioningWarnings";
	public static final String IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID = "facilitycodeAssociatedWithBadgeId";
	public static final String FACILITYCODE_BADGEID_SEPARATOR = "FacilityCodeBadgeIdseparator";
	public static final String DEFAULT_FACILITYCODE_BADGEID_SEPARATOR = "-";
	

	public static final String REQ_HEADER_PUBLIC_KEY = "PublicKey";
	public static final String REQ_HEADER_AUTHORIZATION = "Authorization";
	public static final String AUTHORIZATION_TYPE = "Basic ";
	public static final String USERID_PWD_SEPERATOR = ":";
	public static final String CONTENT_TYPE = "Content-Type";
	public static final String ACCEPT_TYPE = "Accept";
	public static final String JSON_CONTENT = "application/json";

	// GENERIC METADATA
	public static final String REQ_METHOD_TYPE_GET = "GET";
	public static final String REQ_METHOD_TYPE_POST = "POST";
	public static final String REQ_METHOD_TYPE_HEAD = "HEAD";
	public static final String REQ_METHOD_TYPE_OPTIONS = "OPTIONS";
	public static final String REQ_METHOD_TYPE_PUT = "PUT";
	public static final String REQ_METHOD_TYPE_DELETE = "DELETE";
	public static final String REQ_METHOD_TYPE_TRACE = "TRACE";

	public static final String ATTR_TYPE_STRING = "String";
	public static final String ATTR_TYPE_INT = "Int";
	public static final String ATTR_TYPE_BOOLEAN = "Boolean";
	public static final String ATTR_TYPE_DATE = "Date";

	// API ENDPOINTS
	public static final String ENDPOINT_CARDHOLDERS = "Cardholders";
	public static final String ENDPOINT_CARDS = "Cards";
	public static final String ENDPOINT_BADGES = "Badges";
	public static final String ENDPOINT_BADGETYPES = "BadgeTypes";
	public static final String ENDPOINT_ACCESS_LEVELS = "AccessLevels";

	// CONNECTOR ATTRIBUTES
	public static final String ATTR_USER_LN = "LastName";
	public static final String ATTR_USER_FN = "FirstName";
	public static final String ATTR_USER_MEMEBER_OF_ALL_SITES = "MemberOfAllSites";
	public static final String ATTR_USER_STATTUS = "CardholderStatus";
	public static final String ATTR_USER_ID = "CardholderId";// UUID
	public static final String ATTR_USER_COMPANY_ID = "CompanyId";
	public static final String ATTR_USER_MIDDLE_INITIAL = "MiddleInitial";
	public static final String ATTR_USER_ACTIVE_DATE = "CardholderActiveDate";
	public static final String ATTR_USER_EXPIRY_DATE = "CardholderExpireDate";
	public static final String ATTR_USER_IMAGETYPE = "ImageType";
	public static final String ATTR_USER_IMAGE_DATA = "ImageData";
	public static final String ATTR_USER_ROLE_ID = "CardholderAccessLevelID";// UUID
	public static final String ATTR_USER_ROLE_ACTIVE_DATE = "ActivateDate";
	public static final String ATTR_USER_ROLE_EXPITY_DATE = "DeactivateDate";
	public static final String ATTR_USER_DEPARTMENT = "Department";
	public static final String ATTR_USER_USERTEXT1 = "UserText1";
	public static final String ATTR_USER_USERTEXT2 = "UserText2";
	public static final String ATTR_USER_USERTEXT3 = "UserText3";
	public static final String ATTR_USER_USERTEXT4 = "UserText4";
	public static final String ATTR_USER_USERTEXT5 = "UserText5";
	public static final String ATTR_USER_USERTEXT6 = "UserText6";
	public static final String ATTR_USER_USERTEXT7 = "UserText7";
	public static final String ATTR_USER_USERTEXT8 = "UserText8";
	public static final String ATTR_USER_USERTEXT9 = "UserText9";
	public static final String ATTR_USER_USERTEXT10 = "UserText10";
	public static final String ATTR_USER_USERTEXT11 = "UserText11";
	public static final String ATTR_USER_USERTEXT12 = "UserText12";
	public static final String ATTR_USER_USERTEXT13 = "UserText13";
	public static final String ATTR_USER_USERTEXT14 = "UserText14";
	public static final String ATTR_USER_USERTEXT15 = "UserText15";
	public static final String ATTR_USER_USERTEXT16 = "UserText16";
	public static final String ATTR_USER_USERTEXT17 = "UserText17";
	public static final String ATTR_USER_USERTEXT18 = "UserText18";
	public static final String ATTR_USER_USERTEXT19 = "UserText19";
	public static final String ATTR_USER_USERTEXT20 = "UserText20";
	public static final String ATTR_USER_USERDATE1 = "UserDate1";
	public static final String ATTR_USER_USERDATE2 = "UserDate2";
	public static final String ATTR_USER_USERDATE3 = "UserDate3";
	public static final String ATTR_USER_USERDATE4 = "UserDate4";
	public static final String ATTR_USER_USERDATE5 = "UserDate5";
	public static final String ATTR_USER_USERNUMERIC1 = "UserNumeric1";
	public static final String ATTR_USER_USERNUMERIC2 = "UserNumeric2";
	public static final String ATTR_USER_USERNUMERIC3 = "UserNumeric3";
	public static final String ATTR_USER_USERNUMERIC4 = "UserNumeric4";
	public static final String ATTR_USER_USERNUMERIC5 = "UserNumeric5";

	public static final String ATTR_ROLE_ID = "AccessLevelID";// UUID
	public static final String ATTR_ROLE_NAME = "AccessLevelName";

	// optional and important fields for badge
	public static final String ATTR_BADGE_ID = "CardId";// UUID
	public static final String ATTR_BADGE_ACTIVE_DATE = "ActiveDate";
	public static final String ATTR_BADGE_EXPIRY_DATE = "ExpireDate";
	public static final String ATTR_BADGE_IP_LOCKSET_USERTYPE = "IpLocksetUserType";
	public static final String ATTR_BADGE_IP_LOCKSET_ACCESS_MODE = "IpLocksetAccessMode";
	public static final String ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS = "IpLocksetAccessAlways";
	public static final String ATTR_BADGE_TYPE = "BadgeType";
	public static final String ATTR_BADGE_DATE_ISSUED = "DateIssued";
	public static final String ATTR_BADGE_ISSUED_BY = "IssuedBy";
	public static final String ATTR_BADGE_NO_OF_TIMES_PRINTED = "NumberOfTimesPrinted";
	public static final String ATTR_BADGE_CREATED_DATE = "DateCreated";
	public static final String ATTR_BADGE_CREATED_BY_USER = "CreatedByUser";
	public static final String ATTR_BADGE_LAST_MODIFIED = "LastModified ";
	public static final String ATTR_BADGE_LAST_MODIFIED_BY_USER = "LastModifiedByUser";
	// mandatory fields to create Badge
	public static final String ATTR_BADGE_NUMBER = "CardNumber";
	public static final String ATTR_BADGE_FACILITY_CODE = "FacilityCode";
	public static final String ATTR_BADGE_PIN_NUMBER = "PinNumber";
	public static final String ATTR_BADGE_PIN_EXCEMPT = "PinExempt";
	public static final String ATTR_BADGE_APBEXCEMPT = "ApbExempt";
	public static final String ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES = "UseExtendedAccessTimes";
	public static final String ATTR_BADGE_STATUS = "CardStatus";
	public static final String ATTR_BADGE_USER_LEVEL = "UserLevel";
	public static final String ATTR_BADGE_USER_CUSTOM_REPORTING = "UseCustomReporting";
	public static final String ATTR_BADGE_ISSUE_LEVEL = "IssueLevel";
	public static final String ATTR_BADGE_DEACTIVATE_EXCEMPT = "DeactivateExempt";
	public static final String ATTR_BADGE_USE_COUNT = "UseCount";
	
	
	public static final String ATTR_NOTES = "Notes";

	// METADATA FOR CONNECTOR ATTRIBUTES
	public static final int CARDHOLDER_STATUS_INACTIVE = 0;
	public static final int CARDHOLDER_STATUS_ACTIVE = 1;
	public static final int CARDHOLDER_STATUS_DATEBASED = 2;
	public static final String CARDHOLDER_STATUS_ACTIVE_STRING = "Active";
	public static final String CARDHOLDER_STATUS_INACTIVE_STRING = "Inactive";
	public static final String CARDHOLDER_STATUS_DATEBASED_STRING = "Datebased";

	public static final int CARD_STATUS_ACTIVE = 1;
	public static final int CARD_STATUS_INACTIVE = 0;
	public static final int CARD_STATUS_DATEBASED = 2;
	public static final String CARD_STATUS_ACTIVE_STRING = "Active";
	public static final String CARD_STATUS_INACTIVE_STRING = "Inactive";
	public static final String CARD_STATUS_DATEBASED_STRING = "Datebased";

	public static final Map<Integer, String> CARD_STATUS_MAP = Stream
			.of(new Object[][] { { CARD_STATUS_ACTIVE, CARD_STATUS_ACTIVE_STRING }, { CARD_STATUS_INACTIVE, CARD_STATUS_INACTIVE_STRING }, { CARD_STATUS_DATEBASED, CARD_STATUS_DATEBASED }, })
			.collect(Collectors.toMap(data -> (Integer) data[0], data -> (String) data[1]));

	public static final int CARD_IPLOCK_USERTYPE_MASTER_USER = 1;
	public static final int CARD_IPLOCK_USERTYPE_EMERGENCY_USER = 2;
	public static final int CARD_IPLOCK_USERTYPE_DEADBOLTOVERRIDE = 3;
	public static final int CARD_IPLOCK_USERTYPE_REGULARUSER = 4;
	public static final int CARD_IPLOCK_USERTYPE_EXTENDEDUSER = 5;
	public static final int CARD_IPLOCK_USERTYPE_PASSAGEUSER = 6;
	public static final int CARD_IPLOCK_USERTYPE_USELIMITUSER = 7;
	public static final int CARD_IPLOCK_USERTYPE_PANICUSER = 8;
	public static final int CARD_IPLOCK_USERTYPE_LOCKOUTUSER = 9;
	public static final int CARD_IPLOCK_USERTYPE_RELOCKUSER = 10;
	public static final int CARD_IPLOCK_USERTYPE_NOTIFYUSER = 11;
	public static final int CARD_IPLOCK_USERTYPE_COMMUSER = 12;
	public static final int CARD_IPLOCK_USERTYPE_SUSPENDEDUSER = 13;
	public static final String CARD_IPLOCK_USERTYPE_MASTER_USER_STRING = "MasterUser";
	public static final String CARD_IPLOCK_USERTYPE_EMERGENCY_USER_STRING = "EmergencyUser";
	public static final String CARD_IPLOCK_USERTYPE_DEADBOLTOVERRIDE_STRING = "DeadboltOverride";
	public static final String CARD_IPLOCK_USERTYPE_REGULARUSER_STRING = "RegularUser";
	public static final String CARD_IPLOCK_USERTYPE_EXTENDEDUSER_STRING = "ExtendedUser";
	public static final String CARD_IPLOCK_USERTYPE_PASSAGEUSER_STRING = "PassageUser";
	public static final String CARD_IPLOCK_USERTYPE_USELIMITUSER_STRING = "UseLimitUser";
	public static final String CARD_IPLOCK_USERTYPE_PANICUSER_STRING = "PanicUser";
	public static final String CARD_IPLOCK_USERTYPE_LOCKOUTUSER_STRING = "LockoutUser";
	public static final String CARD_IPLOCK_USERTYPE_RELOCKUSER_STRING = "RelockUser";
	public static final String CARD_IPLOCK_USERTYPE_NOTIFYUSER_STRING = "NotifyUser";
	public static final String CARD_IPLOCK_USERTYPE_COMMUSER_STRING = "CommUser";
	public static final String CARD_IPLOCK_USERTYPE_SUSPENDEDUSER_STRING = "SuspendedUser";

	public static final int CARD_IPLOCK_MODE_CARD_ONLY = 1;
	public static final int CARD_IPLOCK_MODE_CARD_OR_PIN = 2;
	public static final int CARD_IPLOCK_MODE_CARD_AND_PIN = 3;
	public static final int CARD_IPLOCK_MODE_CARD_THEN_PIN = 4;
	public static final String CARD_IPLOCK_MODE_CARD_ONLY_STRING = "CardOnly";
	public static final String CARD_IPLOCK_MODE_CARD_OR_PIN_STRING = "CardOrPIN";
	public static final String CARD_IPLOCK_MODE_CARD_AND_PIN_STRING = "CardAndPIN";
	public static final String CARD_IPLOCK_MODE_CARD_THEN_PIN_STRING = "CardThenPIN";

	public static final int USER_IMAGE_TYPE_PHOTO = 1;
	public static final int USER_IMAGE_TYPE_SIGNATURE = 2;
	public static final int USER_IMAGE_TYPE_SUPREMA = 3;
	public static final int USER_IMAGE_TYPE_RSI = 101;
	public static final int USER_IMAGE_TYPE_INDENTIX = 102;
	public static final int USER_IMAGE_TYPE_BIOSCRIPT = 103;
	public static final int USER_IMAGE_TYPE_IRIDIAN = 104;
	public static final int USER_IMAGE_TYPE_OSDP_1 = 105;
	public static final int USER_IMAGE_TYPE_OSDP_2 = 106;
	public static final int USER_IMAGE_TYPE_OSDP_3 = 107;
	public static final int USER_IMAGE_TYPE_OSDP_4 = 108;

	public static final String USER_IMAGE_TYPE_PHOTO_STRING = "Photo";
	public static final String USER_IMAGE_TYPE_SIGNATURE_STRING = "SIganture";
	public static final String USER_IMAGE_TYPE_SUPREMA_STRING = "Suprema";
	public static final String USER_IMAGE_TYPE_RSI_STRING = "RSI";
	public static final String USER_IMAGE_TYPE_INDENTIX_STRING = "Indentix";
	public static final String USER_IMAGE_TYPE_BIOSCRIP_STRING = "Bioscrip";
	public static final String USER_IMAGE_TYPE_IRIDIAN_STRING = "Iridian";
	public static final String USER_IMAGE_TYPE_OSDP_1_STRING = "OSDP_1";
	public static final String USER_IMAGE_TYPE_OSDP_2_STRING = "OSDP_2";
	public static final String USER_IMAGE_TYPE_OSDP_3_STRING = "OSDP_3";
	public static final String USER_IMAGE_TYPE_OSDP_4_STRING = "OSDP_4";

}
