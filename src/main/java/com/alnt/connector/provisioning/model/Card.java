/**
 * soori
 * 2019-11-29 
 */
package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author soori
 *
 */
public class Card implements Serializable {

	private static final long serialVersionUID = -7289532378210353969L;

	private String CardId;
	private String CardholderId;
	private Long CardNumber;
	private Integer FacilityCode;
	private Integer PinNumber;
	private Boolean PinExempt;
	private Boolean ApbExempt;
	private Boolean UseExtendedAccessTimes;
	private Integer CardStatus;
	private String ActiveDate;
	private String ExpireDate;
	private Integer UserLevel;
	private Boolean UseCustomReporting;
	private String Notes;
	private String LastModified;
	private String LastModifiedByUser;
	private String DateCreated;
	private String CreatedByUser;
	private String LastCardModRowVersion;
	private Integer IssueLevel;
	private Boolean DeactivateExempt;
	private String VacationDate;
	private Integer VacationDuration;
	private Integer UseCount;
	private String TempDeactivateStart;
	private String TempDeactivateEnd;
	private String Classification;
	private Integer IpLocksetUserType;
	private Integer IpLocksetAccessMode;
	private Integer IpLocksetCredentialFormat;
	private Boolean IpLocksetAccessAlways;
	private String RawPrimaryCredential;
	private String LargeEncodedCardId;
	private String EmbossedNumber;
	private String BadgeType;
	private String DateIssued;
	private String IssuedBy;
	private Integer NumberOfTimesPrinted;
	private String LastPlaceUsed;
	private String LastTimeUsed;
	private String LastActivity;
	private String EngageCredentialFormatID;
	private String EngageCredentialFunction;
	private List<CardAccessLevel> CardAccessLevels;

	public String getCardId() {
		return CardId;
	}

	public void setCardId(String cardId) {
		CardId = cardId;
	}

	public String getCardholderId() {
		return CardholderId;
	}

	public void setCardholderId(String cardholderId) {
		CardholderId = cardholderId;
	}

	public Long getCardNumber() {
		return CardNumber;
	}

	public void setCardNumber(Long cardNumber) {
		CardNumber = cardNumber;
	}

	public Integer getFacilityCode() {
		return FacilityCode;
	}

	public void setFacilityCode(Integer facilityCode) {
		FacilityCode = facilityCode;
	}

	public Integer getPinNumber() {
		return PinNumber;
	}

	public void setPinNumber(Integer pinNumber) {
		PinNumber = pinNumber;
	}

	public Boolean getPinExempt() {
		return PinExempt;
	}

	public void setPinExempt(Boolean pinExempt) {
		PinExempt = pinExempt;
	}

	public Boolean getUseExtendedAccessTimes() {
		return UseExtendedAccessTimes;
	}

	public void setUseExtendedAccessTimes(Boolean useExtendedAccessTimes) {
		UseExtendedAccessTimes = useExtendedAccessTimes;
	}

	public Integer getCardStatus() {
		return CardStatus;
	}

	public void setCardStatus(Integer cardStatus) {
		CardStatus = cardStatus;
	}

	public String getActiveDate() {
		return ActiveDate;
	}

	public void setActiveDate(String activeDate) {
		ActiveDate = activeDate;
	}

	public String getExpireDate() {
		return ExpireDate;
	}

	public void setExpireDate(String expireDate) {
		ExpireDate = expireDate;
	}

	public Integer getUserLevel() {
		return UserLevel;
	}

	public void setUserLevel(Integer userLevel) {
		UserLevel = userLevel;
	}

	public Boolean getUseCustomReporting() {
		return UseCustomReporting;
	}

	public void setUseCustomReporting(Boolean useCustomReporting) {
		UseCustomReporting = useCustomReporting;
	}

	public String getNotes() {
		return Notes;
	}

	public void setNotes(String notes) {
		Notes = notes;
	}

	public String getLastModified() {
		return LastModified;
	}

	public void setLastModified(String lastModified) {
		LastModified = lastModified;
	}

	public String getLastModifiedByUser() {
		return LastModifiedByUser;
	}

	public void setLastModifiedByUser(String lastModifiedByUser) {
		LastModifiedByUser = lastModifiedByUser;
	}

	public String getDateCreated() {
		return DateCreated;
	}

	public void setDateCreated(String dateCreated) {
		DateCreated = dateCreated;
	}

	public String getCreatedByUser() {
		return CreatedByUser;
	}

	public void setCreatedByUser(String createdByUser) {
		CreatedByUser = createdByUser;
	}

	public String getLastCardModRowVersion() {
		return LastCardModRowVersion;
	}

	public void setLastCardModRowVersion(String lastCardModRowVersion) {
		LastCardModRowVersion = lastCardModRowVersion;
	}

	public Integer getIssueLevel() {
		return IssueLevel;
	}

	public void setIssueLevel(Integer issueLevel) {
		IssueLevel = issueLevel;
	}

	public Boolean getDeactivateExempt() {
		return DeactivateExempt;
	}

	public void setDeactivateExempt(Boolean deactivateExempt) {
		DeactivateExempt = deactivateExempt;
	}

	public String getVacationDate() {
		return VacationDate;
	}

	public void setVacationDate(String vacationDate) {
		VacationDate = vacationDate;
	}

	public Integer getVacationDuration() {
		return VacationDuration;
	}

	public void setVacationDuration(Integer vacationDuration) {
		VacationDuration = vacationDuration;
	}

	public Integer getUseCount() {
		return UseCount;
	}

	public void setUseCount(Integer useCount) {
		UseCount = useCount;
	}

	public String getTempDeactivateStart() {
		return TempDeactivateStart;
	}

	public void setTempDeactivateStart(String tempDeactivateStart) {
		TempDeactivateStart = tempDeactivateStart;
	}

	public String getTempDeactivateEnd() {
		return TempDeactivateEnd;
	}

	public void setTempDeactivateEnd(String tempDeactivateEnd) {
		TempDeactivateEnd = tempDeactivateEnd;
	}

	public String getClassification() {
		return Classification;
	}

	public void setClassification(String classification) {
		Classification = classification;
	}

	public Integer getIpLocksetUserType() {
		return IpLocksetUserType;
	}

	public void setIpLocksetUserType(Integer ipLocksetUserType) {
		IpLocksetUserType = ipLocksetUserType;
	}

	public Integer getIpLocksetAccessMode() {
		return IpLocksetAccessMode;
	}

	public void setIpLocksetAccessMode(Integer ipLocksetAccessMode) {
		IpLocksetAccessMode = ipLocksetAccessMode;
	}

	public Integer getIpLocksetCredentialFormat() {
		return IpLocksetCredentialFormat;
	}

	public void setIpLocksetCredentialFormat(Integer ipLocksetCredentialFormat) {
		IpLocksetCredentialFormat = ipLocksetCredentialFormat;
	}

	public Boolean getIpLocksetAccessAlways() {
		return IpLocksetAccessAlways;
	}

	public void setIpLocksetAccessAlways(Boolean ipLocksetAccessAlways) {
		IpLocksetAccessAlways = ipLocksetAccessAlways;
	}

	public String getRawPrimaryCredential() {
		return RawPrimaryCredential;
	}

	public void setRawPrimaryCredential(String rawPrimaryCredential) {
		RawPrimaryCredential = rawPrimaryCredential;
	}

	public String getLargeEncodedCardId() {
		return LargeEncodedCardId;
	}

	public void setLargeEncodedCardId(String largeEncodedCardId) {
		LargeEncodedCardId = largeEncodedCardId;
	}

	public String getEmbossedNumber() {
		return EmbossedNumber;
	}

	public void setEmbossedNumber(String embossedNumber) {
		EmbossedNumber = embossedNumber;
	}

	public String getBadgeType() {
		return BadgeType;
	}

	public void setBadgeType(String badgeType) {
		BadgeType = badgeType;
	}

	public String getDateIssued() {
		return DateIssued;
	}

	public void setDateIssued(String dateIssued) {
		DateIssued = dateIssued;
	}

	public String getIssuedBy() {
		return IssuedBy;
	}

	public void setIssuedBy(String issuedBy) {
		IssuedBy = issuedBy;
	}

	public Integer getNumberOfTimesPrinted() {
		return NumberOfTimesPrinted;
	}

	public void setNumberOfTimesPrinted(Integer numberOfTimesPrinted) {
		NumberOfTimesPrinted = numberOfTimesPrinted;
	}

	public String getLastPlaceUsed() {
		return LastPlaceUsed;
	}

	public void setLastPlaceUsed(String lastPlaceUsed) {
		LastPlaceUsed = lastPlaceUsed;
	}

	public String getLastTimeUsed() {
		return LastTimeUsed;
	}

	public void setLastTimeUsed(String lastTimeUsed) {
		LastTimeUsed = lastTimeUsed;
	}

	public String getLastActivity() {
		return LastActivity;
	}

	public void setLastActivity(String lastActivity) {
		LastActivity = lastActivity;
	}

	public String getEngageCredentialFormatID() {
		return EngageCredentialFormatID;
	}

	public void setEngageCredentialFormatID(String engageCredentialFormatID) {
		EngageCredentialFormatID = engageCredentialFormatID;
	}

	public String getEngageCredentialFunction() {
		return EngageCredentialFunction;
	}

	public void setEngageCredentialFunction(String engageCredentialFunction) {
		EngageCredentialFunction = engageCredentialFunction;
	}

	public List<CardAccessLevel> getCardAccessLevels() {
		return CardAccessLevels;
	}

	public void setCardAccessLevels(List<CardAccessLevel> cardAccessLevels) {
		CardAccessLevels = cardAccessLevels;
	}
	
	public Boolean getApbExempt() {
		return ApbExempt;
	}

	public void setApbExempt(Boolean apbExempt) {
		ApbExempt = apbExempt;
	}

	@Override
	public String toString() {
		return "Card [CardId=" + CardId + ", CardholderId=" + CardholderId + ", CardNumber=" + CardNumber + ", FacilityCode=" + FacilityCode + ", PinNumber=" + PinNumber + ", PinExempt=" + PinExempt + ", ApbExempt=" + ApbExempt
				+ ", UseExtendedAccessTimes=" + UseExtendedAccessTimes + ", CardStatus=" + CardStatus + ", ActiveDate=" + ActiveDate + ", ExpireDate=" + ExpireDate + ", UserLevel=" + UserLevel + ", UseCustomReporting=" + UseCustomReporting
				+ ", Notes=" + Notes + ", LastModified=" + LastModified + ", LastModifiedByUser=" + LastModifiedByUser + ", DateCreated=" + DateCreated + ", CreatedByUser=" + CreatedByUser + ", LastCardModRowVersion="
				+ LastCardModRowVersion + ", IssueLevel=" + IssueLevel + ", DeactivateExempt=" + DeactivateExempt + ", VacationDate=" + VacationDate + ", VacationDuration=" + VacationDuration + ", UseCount=" + UseCount
				+ ", TempDeactivateStart=" + TempDeactivateStart + ", TempDeactivateEnd=" + TempDeactivateEnd + ", Classification=" + Classification + ", IpLocksetUserType=" + IpLocksetUserType + ", IpLocksetAccessMode="
				+ IpLocksetAccessMode + ", IpLocksetCredentialFormat=" + IpLocksetCredentialFormat + ", IpLocksetAccessAlways=" + IpLocksetAccessAlways + ", RawPrimaryCredential=" + RawPrimaryCredential + ", LargeEncodedCardId="
				+ LargeEncodedCardId + ", EmbossedNumber=" + EmbossedNumber + ", BadgeType=" + BadgeType + ", DateIssued=" + DateIssued + ", IssuedBy=" + IssuedBy + ", NumberOfTimesPrinted=" + NumberOfTimesPrinted + ", LastPlaceUsed="
				+ LastPlaceUsed + ", LastTimeUsed=" + LastTimeUsed + ", LastActivity=" + LastActivity + ", EngageCredentialFormatID=" + EngageCredentialFormatID + ", EngageCredentialFunction=" + EngageCredentialFunction
				+ ", CardAccessLevels=" + CardAccessLevels + ", getCardId()=" + getCardId() + ", getCardholderId()=" + getCardholderId() + ", getCardNumber()=" + getCardNumber() + ", getFacilityCode()=" + getFacilityCode()
				+ ", getPinNumber()=" + getPinNumber() + ", getPinExempt()=" + getPinExempt() + ", getUseExtendedAccessTimes()=" + getUseExtendedAccessTimes() + ", getCardStatus()=" + getCardStatus() + ", getActiveDate()=" + getActiveDate()
				+ ", getExpireDate()=" + getExpireDate() + ", getUserLevel()=" + getUserLevel() + ", getUseCustomReporting()=" + getUseCustomReporting() + ", getNotes()=" + getNotes() + ", getLastModified()=" + getLastModified()
				+ ", getLastModifiedByUser()=" + getLastModifiedByUser() + ", getDateCreated()=" + getDateCreated() + ", getCreatedByUser()=" + getCreatedByUser() + ", getLastCardModRowVersion()=" + getLastCardModRowVersion()
				+ ", getIssueLevel()=" + getIssueLevel() + ", getDeactivateExempt()=" + getDeactivateExempt() + ", getVacationDate()=" + getVacationDate() + ", getVacationDuration()=" + getVacationDuration() + ", getUseCount()="
				+ getUseCount() + ", getTempDeactivateStart()=" + getTempDeactivateStart() + ", getTempDeactivateEnd()=" + getTempDeactivateEnd() + ", getClassification()=" + getClassification() + ", getIpLocksetUserType()="
				+ getIpLocksetUserType() + ", getIpLocksetAccessMode()=" + getIpLocksetAccessMode() + ", getIpLocksetCredentialFormat()=" + getIpLocksetCredentialFormat() + ", getIpLocksetAccessAlways()=" + getIpLocksetAccessAlways()
				+ ", getRawPrimaryCredential()=" + getRawPrimaryCredential() + ", getLargeEncodedCardId()=" + getLargeEncodedCardId() + ", getEmbossedNumber()=" + getEmbossedNumber() + ", getBadgeType()=" + getBadgeType()
				+ ", getDateIssued()=" + getDateIssued() + ", getIssuedBy()=" + getIssuedBy() + ", getNumberOfTimesPrinted()=" + getNumberOfTimesPrinted() + ", getLastPlaceUsed()=" + getLastPlaceUsed() + ", getLastTimeUsed()="
				+ getLastTimeUsed() + ", getLastActivity()=" + getLastActivity() + ", getEngageCredentialFormatID()=" + getEngageCredentialFormatID() + ", getEngageCredentialFunction()=" + getEngageCredentialFunction()
				+ ", getCardAccessLevels()=" + getCardAccessLevels() + ", getApbExempt()=" + getApbExempt() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	

}
