/**
 * soori
 * 2019-11-29 
 */
package com.alnt.connector.provisioning.model;

import java.io.Serializable;

/**
 * @author soori
 *
 */
public class CardAccessLevel implements Serializable {
	
	private static final long serialVersionUID = 2501382350488683749L;
	
	private String CardAccessLevelId;
	private String CardID;
	private String AccessLevelID;
	private String LastModified;
	private String ActivateDate;
	private String DeactivateDate;
	
	public String getCardAccessLevelId() {
		return CardAccessLevelId;
	}
	public void setCardAccessLevelId(String cardAccessLevelId) {
		CardAccessLevelId = cardAccessLevelId;
	}
	public String getCardID() {
		return CardID;
	}
	public void setCardID(String cardID) {
		CardID = cardID;
	}
	public String getAccessLevelID() {
		return AccessLevelID;
	}
	public void setAccessLevelID(String accessLevelID) {
		AccessLevelID = accessLevelID;
	}
	public String getLastModified() {
		return LastModified;
	}
	public void setLastModified(String lastModified) {
		LastModified = lastModified;
	}
	public String getActivateDate() {
		return ActivateDate;
	}
	public void setActivateDate(String activateDate) {
		ActivateDate = activateDate;
	}
	public String getDeactivateDate() {
		return DeactivateDate;
	}
	public void setDeactivateDate(String deactivateDate) {
		DeactivateDate = deactivateDate;
	}
	
	@Override
	public String toString() {
		return "CardAccessLevel [CardAccessLevelId=" + CardAccessLevelId + ", CardID=" + CardID + ", AccessLevelID="
				+ AccessLevelID + ", LastModified=" + LastModified + ", ActivateDate=" + ActivateDate
				+ ", DeactivateDate=" + DeactivateDate + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((AccessLevelID == null) ? 0 : AccessLevelID.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardAccessLevel other = (CardAccessLevel) obj;
		if (AccessLevelID == null) {
			if (other.AccessLevelID != null)
				return false;
		} else if (!AccessLevelID.equals(other.AccessLevelID))
			return false;
		return true;
	}
	
	
	
	

}
