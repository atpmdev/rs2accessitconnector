package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.alnt.extractionconnector.common.model.IUserInformation;

public class UserInformation implements Serializable, IUserInformation {

	private static final long serialVersionUID = -837469157659422986L;
	private Map<String, Object> userDetails;
	private Map<String, Map<String, List<Map<String, Object>>>> entitlements;

	public Map<String, Object> getUserDetails() {
		return userDetails;
	}

	public Map<String, Map<String, List<Map<String, Object>>>> getEntitlements() {
		return entitlements;
	}

	public void setUserDetails(Map<String, Object> userDetails) {
		this.userDetails = userDetails;
	}

	public void setEntitlements(Map<String, Map<String, List<Map<String, Object>>>> entitlements) {
		this.entitlements = entitlements;
	}

}
