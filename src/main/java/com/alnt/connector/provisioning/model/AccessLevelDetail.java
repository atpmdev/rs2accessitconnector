/**
 * soori
 * 2019-11-25 
 */
package com.alnt.connector.provisioning.model;

import java.io.Serializable;

/**
 * @author soori
 *
 */
public class AccessLevelDetail implements Serializable {

	private static final long serialVersionUID = -7049104975658574410L;

	private String AccessLevelDetailID;
	private String AccessLevelID;
	private String ReaderID;
	private String TimezoneID;
	private Integer OperatingMode;

	public String getAccessLevelDetailID() {
		return AccessLevelDetailID;
	}

	public void setAccessLevelDetailID(String accessLevelDetailID) {
		AccessLevelDetailID = accessLevelDetailID;
	}

	public String getAccessLevelID() {
		return AccessLevelID;
	}

	public void setAccessLevelID(String accessLevelID) {
		AccessLevelID = accessLevelID;
	}

	public String getReaderID() {
		return ReaderID;
	}

	public void setReaderID(String readerID) {
		ReaderID = readerID;
	}

	public String getTimezoneID() {
		return TimezoneID;
	}

	public void setTimezoneID(String timezoneID) {
		TimezoneID = timezoneID;
	}

	public Integer getOperatingMode() {
		return OperatingMode;
	}

	public void setOperatingMode(Integer operatingMode) {
		OperatingMode = operatingMode;
	}

}
