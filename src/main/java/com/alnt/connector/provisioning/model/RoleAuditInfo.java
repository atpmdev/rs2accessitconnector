package com.alnt.connector.provisioning.model;

import java.util.Date;

import com.alnt.access.provisioning.model.IRoleAuditInfo;

public class RoleAuditInfo implements IRoleAuditInfo {
	
	String roleName,action;
	Date validFrom,validTo;
	String enterpriseRoleName;

	public String getAction() {		
		return action;
	}

	public String getRoleName() {		
		return roleName;
	}

	public Date getValidFrom() {		
		return validFrom;
	}

	public Date getValidTo() {		
		return validTo;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;

	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}


}
