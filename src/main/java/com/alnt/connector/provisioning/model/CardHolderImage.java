package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class CardHolderImage implements Serializable {
	
	private static final long serialVersionUID = -4019284752821180443L;
	
	private String ImageID;
	private String CardholderID;
	//required
	private Integer ImageType;  //1 = Photo, 2 = Signature, 3 = Suprema, 101 = RSI, 102 = Identix, 103 = Bioscrypt, 104 = Iridian, 105 = OSDP_1, 106 = OSDP_2, 107 = OSDP_3, 108 = OSDP_4 = ['1', '2', '3', '101', '102', '103', '104', '105', '106', '107', '108'],
	//required
	private String ImageData;//The Image Data as byte-array ,
	private String LastModified;
	
	
	public String getImageID() {
		return ImageID;
	}
	public void setImageID(String imageID) {
		ImageID = imageID;
	}
	public String getCardholderID() {
		return CardholderID;
	}
	public void setCardholderID(String cardholderID) {
		CardholderID = cardholderID;
	}
	public Integer getImageType() {
		return ImageType;
	}
	public void setImageType(Integer imageType) {
		ImageType = imageType;
	}
	public String getImageData() {
		return ImageData;
	}
	public void setImageData(String imageData) {
		ImageData = imageData;
	}
	public String getLastModified() {
		return LastModified;
	}
	public void setLastModified(String lastModified) {
		LastModified = lastModified;
	}

}
