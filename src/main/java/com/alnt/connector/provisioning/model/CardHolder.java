package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.List;
public class CardHolder implements Serializable {
	
	private static final long serialVersionUID = 5412560064239914240L;
	
	
	private String  CardholderId; 
	// required
	private String LastName;;
	// required
	private String FirstName;
	private String MiddleInitial;;
	private String CompanyId;
	// required
	private Boolean MemberOfAllSites = false;
	private String Notes;
	// required
	private Integer CardholderStatus = 1;// 0 = Inactive, 1 = Active, 2 = DateBased = ['0', '1', '2'],
	private String CardholderActiveDate;
	private String CardholderExpireDate;
	private List<CardHolderImage> Images;
	private List<CardholderAccessLevel> CardholderAccessLevels;
	private List<CardholderSite> CardholderSites;
	private CardHolderUserColumns UserColumns;
	

	public String getCardholderId() {
		return CardholderId;
	}
	public void setCardholderId(String cardholderId) {
		CardholderId = cardholderId;
	}
	
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getMiddleInitial() {
		return MiddleInitial;
	}
	public void setMiddleInitial(String middleInitial) {
		MiddleInitial = middleInitial;
	}
	public String getCompanyId() {
		return CompanyId;
	}
	public void setCompanyId(String companyId) {
		CompanyId = companyId;
	}
	public Boolean getMemberOfAllSites() {
		return MemberOfAllSites;
	}
	public void setMemberOfAllSites(Boolean memberOfAllSites) {
		MemberOfAllSites = memberOfAllSites;
	}
	public String getNotes() {
		return Notes;
	}
	public void setNotes(String notes) {
		Notes = notes;
	}
	public Integer getCardholderStatus() {
		return CardholderStatus;
	}
	public void setCardholderStatus(Integer cardholderStatus) {
		CardholderStatus = cardholderStatus;
	}
	public String getCardholderActiveDate() {
		return CardholderActiveDate;
	}
	public void setCardholderActiveDate(String cardholderActiveDate) {
		CardholderActiveDate = cardholderActiveDate;
	}
	public String getCardholderExpireDate() {
		return CardholderExpireDate;
	}
	public void setCardholderExpireDate(String cardholderExpireDate) {
		CardholderExpireDate = cardholderExpireDate;
	}
	public List<CardHolderImage> getImages() {
		return Images;
	}
	public void setImages(List<CardHolderImage> images) {
		Images = images;
	}
	public List<CardholderAccessLevel> getCardholderAccessLevels() {
		return CardholderAccessLevels;
	}
	public void setCardholderAccessLevels(List<CardholderAccessLevel> cardholderAccessLevels) {
		CardholderAccessLevels = cardholderAccessLevels;
	}
	public List<CardholderSite> getCardholderSites() {
		return CardholderSites;
	}
	public void setCardholderSites(List<CardholderSite> cardholderSites) {
		CardholderSites = cardholderSites;
	}
	public CardHolderUserColumns getUserColumns() {
		return UserColumns;
	}
	public void setUserColumns(CardHolderUserColumns userColumns) {
		UserColumns = userColumns;
	}
	@Override
	public String toString() {
		return "CardHolder [CardholderId=" + CardholderId + ", LastName=" + LastName + ", FirstName=" + FirstName
				+ ", MiddleInitial=" + MiddleInitial + ", CompanyId=" + CompanyId + ", MemberOfAllSites="
				+ MemberOfAllSites + ", Notes=" + Notes + ", CardholderStatus=" + CardholderStatus
				+ ", CardholderActiveDate=" + CardholderActiveDate + ", CardholderExpireDate=" + CardholderExpireDate
				+ ", Images=" + Images + ", CardholderAccessLevels=" + CardholderAccessLevels + ", CardholderSites="
				+ CardholderSites + "]";
	}
	
	
	
}
