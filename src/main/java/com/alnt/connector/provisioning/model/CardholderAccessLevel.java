package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class CardholderAccessLevel implements  Serializable {
	
	private static final long serialVersionUID = -6493030786844715283L;
	
	private String CardholderAccessLevelID;
	private String CardholderID;
	//required
	private String AccessLevelID;
	private String LastModified;
	private String ActivateDate;
	private String DeactivateDate;
	
	
	public String getCardholderAccessLevelID() {
		return CardholderAccessLevelID;
	}
	public void setCardholderAccessLevelID(String cardholderAccessLevelID) {
		CardholderAccessLevelID = cardholderAccessLevelID;
	}
	public String getCardholderID() {
		return CardholderID;
	}
	public void setCardholderID(String cardholderID) {
		CardholderID = cardholderID;
	}
	public String getAccessLevelID() {
		return AccessLevelID;
	}
	public void setAccessLevelID(String accessLevelID) {
		AccessLevelID = accessLevelID;
	}
	public String getLastModified() {
		return LastModified;
	}
	public void setLastModified(String lastModified) {
		LastModified = lastModified;
	}
	public String getActivateDate() {
		return ActivateDate;
	}
	public void setActivateDate(String activateDate) {
		ActivateDate = activateDate;
	}
	public String getDeactivateDate() {
		return DeactivateDate;
	}
	public void setDeactivateDate(String deactivateDate) {
		DeactivateDate = deactivateDate;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((AccessLevelID == null) ? 0 : AccessLevelID.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CardholderAccessLevel other = (CardholderAccessLevel) obj;
		if (AccessLevelID == null) {
			if (other.AccessLevelID != null)
				return false;
		} else if (!AccessLevelID.equals(other.AccessLevelID))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CardholderAccessLevel [CardholderAccessLevelID=" + CardholderAccessLevelID + ", CardholderID="
				+ CardholderID + ", AccessLevelID=" + AccessLevelID + ", LastModified=" + LastModified
				+ ", ActivateDate=" + ActivateDate + ", DeactivateDate=" + DeactivateDate + "]";
	}

}
