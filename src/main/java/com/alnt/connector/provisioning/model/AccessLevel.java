/**
 * soori
 * 2019-11-25 
 */
package com.alnt.connector.provisioning.model;

import java.io.Serializable;
import java.util.List;

/**
 * @author soori
 *
 */
public class AccessLevel implements Serializable {
	
	private static final long serialVersionUID = 28927798243780704L;
	
	private String  AccessLevelID;
	private String  SiteID;
	private String  CompanyID;
	private String  AccessLevelName;
	private List<AccessLevelDetail> AccessLevelDetails;
	
	public String getAccessLevelID() {
		return AccessLevelID;
	}
	public void setAccessLevelID(String accessLevelID) {
		AccessLevelID = accessLevelID;
	}
	public String getSiteID() {
		return SiteID;
	}
	public void setSiteID(String siteID) {
		SiteID = siteID;
	}
	public String getCompanyID() {
		return CompanyID;
	}
	public void setCompanyID(String companyID) {
		CompanyID = companyID;
	}
	public String getAccessLevelName() {
		return AccessLevelName;
	}
	public void setAccessLevelName(String accessLevelName) {
		AccessLevelName = accessLevelName;
	}
	public List<AccessLevelDetail> getAccessLevelDetails() {
		return AccessLevelDetails;
	}
	public void setAccessLevelDetails(List<AccessLevelDetail> accessLevelDetails) {
		AccessLevelDetails = accessLevelDetails;
	}
	@Override
	public String toString() {
		return "AccessLevel [AccessLevelID=" + AccessLevelID + ", SiteID=" + SiteID + ", CompanyID=" + CompanyID
				+ ", AccessLevelName=" + AccessLevelName + ", AccessLevelDetails=" + AccessLevelDetails + "]";
	}
}
