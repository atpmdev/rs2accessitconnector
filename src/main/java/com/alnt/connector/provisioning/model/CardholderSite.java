package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class CardholderSite implements Serializable {

	private static final long serialVersionUID = 1192223724491557403L;

	private String CardholderSiteID;
	private String CardholderID;
	//required
	private String SiteID;
	private String LastModified;

	public String getCardholderSiteID() {
		return CardholderSiteID;
	}

	public void setCardholderSiteID(String cardholderSiteID) {
		CardholderSiteID = cardholderSiteID;
	}

	public String getCardholderID() {
		return CardholderID;
	}

	public void setCardholderID(String cardholderID) {
		CardholderID = cardholderID;
	}

	public String getSiteID() {
		return SiteID;
	}

	public void setSiteID(String siteID) {
		SiteID = siteID;
	}

	public String getLastModified() {
		return LastModified;
	}

	public void setLastModified(String lastModified) {
		LastModified = lastModified;
	}

}
