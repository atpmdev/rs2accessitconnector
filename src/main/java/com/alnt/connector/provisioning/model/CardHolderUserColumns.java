package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class CardHolderUserColumns implements Serializable {

	private static final long serialVersionUID = -39155405904549684L;
	
	private String Department;
	private String UserText1;
	private String UserText2;
	private String UserText3;
	private String UserText4;
	private String UserText5;
	private String UserText6;
	private String UserText7;
	private String UserText8;
	private String UserText9;
	private String UserText10;
	private String UserText11;
	private String UserText12;
	private String UserText13;
	private String UserText14;
	private String UserText15;
	private String UserText16;
	private String UserText17;
	private String UserText18;
	private String UserText19;
	private String UserText20;
	private String UserDate1;
	private String UserDate2;
	private String UserDate3;
	private String UserDate4;
	private String UserDate5;
	private String UserNumeric1;
	private String UserNumeric2;
	private String UserNumeric3;
	private String UserNumeric4;
	private String UserNumeric5;
	
	
	public String getDepartment() {
		return Department;
	}
	public void setDepartment(String department) {
		Department = department;
	}
	public String getUserText1() {
		return UserText1;
	}
	public void setUserText1(String userText1) {
		UserText1 = userText1;
	}
	public String getUserText2() {
		return UserText2;
	}
	public void setUserText2(String userText2) {
		UserText2 = userText2;
	}
	public String getUserText3() {
		return UserText3;
	}
	public void setUserText3(String userText3) {
		UserText3 = userText3;
	}
	public String getUserText4() {
		return UserText4;
	}
	public void setUserText4(String userText4) {
		UserText4 = userText4;
	}
	public String getUserText5() {
		return UserText5;
	}
	public void setUserText5(String userText5) {
		UserText5 = userText5;
	}
	public String getUserText6() {
		return UserText6;
	}
	public void setUserText6(String userText6) {
		UserText6 = userText6;
	}
	public String getUserText7() {
		return UserText7;
	}
	public void setUserText7(String userText7) {
		UserText7 = userText7;
	}
	public String getUserText8() {
		return UserText8;
	}
	public void setUserText8(String userText8) {
		UserText8 = userText8;
	}
	public String getUserText9() {
		return UserText9;
	}
	public void setUserText9(String userText9) {
		UserText9 = userText9;
	}
	public String getUserText10() {
		return UserText10;
	}
	public void setUserText10(String userText10) {
		UserText10 = userText10;
	}
	public String getUserText11() {
		return UserText11;
	}
	public void setUserText11(String userText11) {
		UserText11 = userText11;
	}
	public String getUserText12() {
		return UserText12;
	}
	public void setUserText12(String userText12) {
		UserText12 = userText12;
	}
	public String getUserText13() {
		return UserText13;
	}
	public void setUserText13(String userText13) {
		UserText13 = userText13;
	}
	public String getUserText14() {
		return UserText14;
	}
	public void setUserText14(String userText14) {
		UserText14 = userText14;
	}
	public String getUserText15() {
		return UserText15;
	}
	public void setUserText15(String userText15) {
		UserText15 = userText15;
	}
	public String getUserText16() {
		return UserText16;
	}
	public void setUserText16(String userText16) {
		UserText16 = userText16;
	}
	public String getUserText17() {
		return UserText17;
	}
	public void setUserText17(String userText17) {
		UserText17 = userText17;
	}
	public String getUserText18() {
		return UserText18;
	}
	public void setUserText18(String userText18) {
		UserText18 = userText18;
	}
	public String getUserText19() {
		return UserText19;
	}
	public void setUserText19(String userText19) {
		UserText19 = userText19;
	}
	public String getUserText20() {
		return UserText20;
	}
	public void setUserText20(String userText20) {
		UserText20 = userText20;
	}
	public String getUserDate1() {
		return UserDate1;
	}
	public void setUserDate1(String userDate1) {
		UserDate1 = userDate1;
	}
	public String getUserDate2() {
		return UserDate2;
	}
	public void setUserDate2(String userDate2) {
		UserDate2 = userDate2;
	}
	public String getUserDate3() {
		return UserDate3;
	}
	public void setUserDate3(String userDate3) {
		UserDate3 = userDate3;
	}
	public String getUserDate4() {
		return UserDate4;
	}
	public void setUserDate4(String userDate4) {
		UserDate4 = userDate4;
	}
	public String getUserDate5() {
		return UserDate5;
	}
	public void setUserDate5(String userDate5) {
		UserDate5 = userDate5;
	}
	public String getUserNumeric1() {
		return UserNumeric1;
	}
	public void setUserNumeric1(String userNumeric1) {
		UserNumeric1 = userNumeric1;
	}
	public String getUserNumeric2() {
		return UserNumeric2;
	}
	public void setUserNumeric2(String userNumeric2) {
		UserNumeric2 = userNumeric2;
	}
	public String getUserNumeric3() {
		return UserNumeric3;
	}
	public void setUserNumeric3(String userNumeric3) {
		UserNumeric3 = userNumeric3;
	}
	public String getUserNumeric4() {
		return UserNumeric4;
	}
	public void setUserNumeric4(String userNumeric4) {
		UserNumeric4 = userNumeric4;
	}
	public String getUserNumeric5() {
		return UserNumeric5;
	}
	public void setUserNumeric5(String userNumeric5) {
		UserNumeric5 = userNumeric5;
	}
	
}
