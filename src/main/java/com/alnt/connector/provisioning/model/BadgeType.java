package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class BadgeType implements Serializable {

	private static final long serialVersionUID = 7023547665311074082L;
	
	private String BadgeTypeId;
	//required
	private String BadgeTypeName;
	
	
	public String getBadgeTypeId() {
		return BadgeTypeId;
	}
	public void setBadgeTypeId(String badgeTypeId) {
		BadgeTypeId = badgeTypeId;
	}
	public String getBadgeTypeName() {
		return BadgeTypeName;
	}
	public void setBadgeTypeName(String badgeTypeName) {
		BadgeTypeName = badgeTypeName;
	}
}
