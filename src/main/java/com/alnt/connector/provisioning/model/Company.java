package com.alnt.connector.provisioning.model;

import java.io.Serializable;

public class Company implements Serializable {

	private static final long serialVersionUID = 3726184858799884592L;

	private String CompanyId;
	private String CompanyName;

	public String getCompanyId() {
		return CompanyId;
	}

	public void setCompanyId(String companyId) {
		CompanyId = companyId;
	}

	public String getCompanyName() {
		return CompanyName;
	}

	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

}
