package com.alnt.connector.provisioning.helper;

import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import com.alnt.connector.constants.RS2AccessItConnectorConstants;

public class  RS2AccessItClientHelper {
	
	final private static String CLASS_NAME = RS2AccessItClientHelper.class.getName();
	static Logger logger = Logger.getLogger(CLASS_NAME);


	public static final boolean isHttpsProtocal(String restUrl) {
		return !restUrl.isEmpty() && restUrl.startsWith("https://");
	}

	public static RS2AccessItURLConnection getConnection(final String restURL, final String methodType,
			final String userId, final String password, String publicKey) {
		if (isHttpsProtocal(restURL)) {
			return getRESTHttpsServiceConnection(restURL.trim(), methodType, userId, password, publicKey);
		} else {
			return getRESTHttpServiceConnection(restURL.trim(), methodType, userId, password, publicKey);
		}
	}

	public static RS2AccessItURLConnection getRESTHttpServiceConnection(final String restURL, final String methodType,
			final String userId, final String password, String publicKey) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(
				CLASS_NAME + "getRESTHttpServiceConnection  :: " + "restUrl" + restURL + "method type: " + methodType);
		RS2AccessItHttpURLConnection restHttpURLConnection = new RS2AccessItHttpURLConnection();
		HttpURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String escapeRestURL=restURL.replaceAll(" ","%20");
			url = new URL(escapeRestURL);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(RS2AccessItConnectorConstants.ACCEPT_TYPE, RS2AccessItConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(RS2AccessItConnectorConstants.CONTENT_TYPE, RS2AccessItConnectorConstants.JSON_CONTENT);
			conn.setDoOutput(true);
			String userpass = userId + RS2AccessItConnectorConstants.USERID_PWD_SEPERATOR + password;
			String basicAuth = RS2AccessItConnectorConstants.AUTHORIZATION_TYPE  + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
			conn.setRequestProperty(RS2AccessItConnectorConstants.REQ_HEADER_AUTHORIZATION, basicAuth);
			conn.setRequestProperty(RS2AccessItConnectorConstants.REQ_HEADER_PUBLIC_KEY, publicKey);
			restHttpURLConnection.setHttpURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		} catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpURLConnection;
	}

	public static RS2AccessItURLConnection getRESTHttpsServiceConnection(final String restURL, final String methodType,
			final String userId, final String password, String publicKey) {
		final String METHOD_NAME = "getRESTServiceConnection()";
		logger.debug(CLASS_NAME + METHOD_NAME + "Inside : getRESTServiceConnection ,restUrl" + restURL + "method type: "
				+ methodType);
		RS2AccessItHttpsURLConnection restHttpsURLConnection = new RS2AccessItHttpsURLConnection();
		HttpsURLConnection conn = null;
		URL url = null;
		try {
			logger.debug(CLASS_NAME + METHOD_NAME + "Building URL:" + restURL + " methodType:" + methodType);
			String escapeRestURL=restURL.replaceAll(" ","%20");
			url = new URL(escapeRestURL);
			logger.debug(CLASS_NAME + METHOD_NAME + "Opening connection");
			conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod(methodType);
			conn.setRequestProperty(RS2AccessItConnectorConstants.ACCEPT_TYPE, RS2AccessItConnectorConstants.JSON_CONTENT);
			conn.setRequestProperty(RS2AccessItConnectorConstants.CONTENT_TYPE, RS2AccessItConnectorConstants.JSON_CONTENT);
			conn.setDoOutput(true);
			String userpass = userId + RS2AccessItConnectorConstants.USERID_PWD_SEPERATOR + password;
			String basicAuth = RS2AccessItConnectorConstants.AUTHORIZATION_TYPE  + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
			conn.setRequestProperty(RS2AccessItConnectorConstants.REQ_HEADER_AUTHORIZATION, basicAuth);
			conn.setRequestProperty(RS2AccessItConnectorConstants.REQ_HEADER_PUBLIC_KEY, publicKey);
			restHttpsURLConnection.setHttpsURLConnection(conn);
			logger.debug(CLASS_NAME + METHOD_NAME + "returning HttpURLConnection connection");
		}catch (Exception ex) {
			logger.error(CLASS_NAME + METHOD_NAME + "Exception while opening the port : " + ex);
		}
		logger.debug(CLASS_NAME + METHOD_NAME + "ENDS");
		return restHttpsURLConnection;
	}
}
