package com.alnt.connector.provisioning.helper;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.alnt.access.provisioning.model.IAttrSearchResult;
import com.alnt.access.provisioning.model.IProvisioningAttributes;
import com.alnt.access.provisioning.model.ProvisioningAttributes;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.provisioning.model.AttrSearchResult;

public class AttributeMetaDataHelper {

	final private static String CLASS_NAME = AttributeMetaDataHelper.class.getName();
	static Logger logger = Logger.getLogger(CLASS_NAME);

	public static List<IAttrSearchResult> getAttributes(final String property) {
		if (property.equals(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS)) {
			return getCardStatusMap();
		} else if (property.equals(RS2AccessItConnectorConstants.ATTR_USER_STATTUS)) {
			return getCardHolderStatusMap();
		} else if (property.equals(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE)) {
			return getIpLocksetAccessModeMap();
		} else if (property.equals(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE)) {
			return getIpLocksetUserTypeMap();
		} else if (property.equals(RS2AccessItConnectorConstants.ATTR_USER_IMAGETYPE)) {
			return getImageTypeMap();
		}
		return new ArrayList<IAttrSearchResult>();
	}


	private static List<IAttrSearchResult> getCardStatusMap() {
		List<IAttrSearchResult> result = new ArrayList<IAttrSearchResult>();
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARDHOLDER_STATUS_INACTIVE), RS2AccessItConnectorConstants.CARDHOLDER_STATUS_INACTIVE_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARDHOLDER_STATUS_ACTIVE), RS2AccessItConnectorConstants.CARDHOLDER_STATUS_ACTIVE_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARDHOLDER_STATUS_DATEBASED), RS2AccessItConnectorConstants.CARDHOLDER_STATUS_DATEBASED_STRING));
		return result;
	}

	private static List<IAttrSearchResult> getCardHolderStatusMap() {
		List<IAttrSearchResult> result = new ArrayList<IAttrSearchResult>();
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_STATUS_INACTIVE), RS2AccessItConnectorConstants.CARD_STATUS_INACTIVE_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_STATUS_ACTIVE), RS2AccessItConnectorConstants.CARD_STATUS_ACTIVE_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_STATUS_DATEBASED), RS2AccessItConnectorConstants.CARD_STATUS_DATEBASED_STRING));
		return result;
	}

	private static List<IAttrSearchResult> getIpLocksetUserTypeMap() {
		List<IAttrSearchResult> result = new ArrayList<IAttrSearchResult>();
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_MASTER_USER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_MASTER_USER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_EMERGENCY_USER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_EMERGENCY_USER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_DEADBOLTOVERRIDE), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_DEADBOLTOVERRIDE_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_REGULARUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_REGULARUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_EXTENDEDUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_EXTENDEDUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_PASSAGEUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_PASSAGEUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_USELIMITUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_USELIMITUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_PANICUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_PANICUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_LOCKOUTUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_LOCKOUTUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_RELOCKUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_RELOCKUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_NOTIFYUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_NOTIFYUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_COMMUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_COMMUSER_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_SUSPENDEDUSER), RS2AccessItConnectorConstants.CARD_IPLOCK_USERTYPE_SUSPENDEDUSER_STRING));
		return result;
	}

	private static List<IAttrSearchResult> getIpLocksetAccessModeMap() {
		List<IAttrSearchResult> result = new ArrayList<IAttrSearchResult>();
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_ONLY), RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_ONLY_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_OR_PIN), RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_OR_PIN_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_AND_PIN), RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_AND_PIN_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_THEN_PIN), RS2AccessItConnectorConstants.CARD_IPLOCK_MODE_CARD_THEN_PIN_STRING));
		return result;
	}

	private static List<IAttrSearchResult> getImageTypeMap() {
		List<IAttrSearchResult> result = new ArrayList<IAttrSearchResult>();
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_PHOTO), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_PHOTO_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_SIGNATURE), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_SIGNATURE_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_SUPREMA), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_SUPREMA_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_RSI), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_RSI_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_INDENTIX), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_INDENTIX_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_BIOSCRIPT), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_BIOSCRIP_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_IRIDIAN), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_IRIDIAN_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_1), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_1_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_2), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_2_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_3), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_3_STRING));
		result.add(new AttrSearchResult(String.valueOf(RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_4), RS2AccessItConnectorConstants.USER_IMAGE_TYPE_OSDP_4_STRING));
		return result;
	}
	
	
	public static List<IProvisioningAttributes> getAttributes() throws Exception {
		List<IProvisioningAttributes> provAttributes = new ArrayList<IProvisioningAttributes>();
		// CARDHOLDER SPECIFIC ATTRIBUTE ::BEGIN
		// User Mandatory Attributes
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_LN, RS2AccessItConnectorConstants.ATTR_USER_LN, true, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_FN, RS2AccessItConnectorConstants.ATTR_USER_FN, true, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_MEMEBER_OF_ALL_SITES, RS2AccessItConnectorConstants.ATTR_USER_MEMEBER_OF_ALL_SITES, true, RS2AccessItConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_STATTUS, RS2AccessItConnectorConstants.ATTR_USER_STATTUS, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		// User Optional Attributes
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_ID, RS2AccessItConnectorConstants.ATTR_USER_ID, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_COMPANY_ID, RS2AccessItConnectorConstants.ATTR_USER_COMPANY_ID, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_DEPARTMENT, RS2AccessItConnectorConstants.ATTR_USER_DEPARTMENT, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_MIDDLE_INITIAL, RS2AccessItConnectorConstants.ATTR_USER_MIDDLE_INITIAL, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE, RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_IMAGETYPE, RS2AccessItConnectorConstants.ATTR_USER_IMAGETYPE, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA, RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT1, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT1, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT2, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT2, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT3, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT3, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT4, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT4, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT5, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT5, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT6, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT6, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT7, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT7, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT8, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT8, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT9, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT9, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT10, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT10, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT11, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT11, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT12, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT12, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT13, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT13, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT14, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT14, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT15, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT15, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT16, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT16, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT17, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT17, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT18, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT18, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT19, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT19, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT20, RS2AccessItConnectorConstants.ATTR_USER_USERTEXT20, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERDATE1, RS2AccessItConnectorConstants.ATTR_USER_USERDATE1, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERDATE2, RS2AccessItConnectorConstants.ATTR_USER_USERDATE2, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERDATE3, RS2AccessItConnectorConstants.ATTR_USER_USERDATE3, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERDATE4, RS2AccessItConnectorConstants.ATTR_USER_USERDATE4, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERDATE5, RS2AccessItConnectorConstants.ATTR_USER_USERDATE5, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC1, RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC1, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC2, RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC2, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC3, RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC3, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC4, RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC4, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC5, RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC5, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		// CARDHOLDER SPECIFIC ATTRIBUTE ::END
		// ROLE Attributes :: BEGIN
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_ROLE_ID, RS2AccessItConnectorConstants.ATTR_ROLE_ID, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_ROLE_NAME, RS2AccessItConnectorConstants.ATTR_ROLE_NAME, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		// ROLE Attributes :: END
		// USER ROLE attributes :: Begin
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_ROLE_ACTIVE_DATE, RS2AccessItConnectorConstants.ATTR_USER_ROLE_ACTIVE_DATE, true, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_USER_ROLE_EXPITY_DATE, RS2AccessItConnectorConstants.ATTR_USER_ROLE_EXPITY_DATE, true, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		// USER ROLE attributes :: Begin
		// CARD ATTRIBUTES ::BEGIN
		// Mandatory
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, true, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, true, RS2AccessItConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, true, RS2AccessItConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes
				.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, true, RS2AccessItConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, true, RS2AccessItConnectorConstants.ATTR_TYPE_BOOLEAN));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, true, RS2AccessItConnectorConstants.ATTR_TYPE_BOOLEAN));
		// OPTIONAL
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_ID, RS2AccessItConnectorConstants.ATTR_BADGE_ID, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE, RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE, RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_TYPE, RS2AccessItConnectorConstants.ATTR_BADGE_TYPE, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_DATE_ISSUED, RS2AccessItConnectorConstants.ATTR_BADGE_DATE_ISSUED, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUED_BY, RS2AccessItConnectorConstants.ATTR_BADGE_ISSUED_BY, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_DATE, RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_DATE, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_BY_USER, RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_BY_USER, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_LAST_MODIFIED, RS2AccessItConnectorConstants.ATTR_BADGE_LAST_MODIFIED, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_LAST_MODIFIED_BY_USER, RS2AccessItConnectorConstants.ATTR_BADGE_LAST_MODIFIED_BY_USER, false, RS2AccessItConnectorConstants.ATTR_TYPE_STRING));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE, RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE, RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_NO_OF_TIMES_PRINTED, RS2AccessItConnectorConstants.ATTR_BADGE_NO_OF_TIMES_PRINTED, false, RS2AccessItConnectorConstants.ATTR_TYPE_INT));
		provAttributes
				.add(new ProvisioningAttributes(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS, RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS, false, RS2AccessItConnectorConstants.ATTR_TYPE_BOOLEAN));
		// CARD ATTRIBUTES :: END
		return provAttributes;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
