package com.alnt.connector.provisioning.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.net.ssl.HttpsURLConnection;

public class RS2AccessItHttpsURLConnection implements RS2AccessItURLConnection {
	private HttpsURLConnection httpsURLConnection;

	public HttpsURLConnection getHttpsURLConnection() {
		return httpsURLConnection;
	}

	public void setHttpsURLConnection(HttpsURLConnection httpsURLConnection) {
		this.httpsURLConnection = httpsURLConnection;
	}

	public int getResponseCode() throws IOException {
		return httpsURLConnection.getResponseCode();
	}

	public void disconnect() {
		httpsURLConnection.disconnect();
	}

	public OutputStream getOutputStream() throws IOException {
		return httpsURLConnection.getOutputStream();
	}

	public InputStream getInputStream() throws IOException {
		return httpsURLConnection.getInputStream();
	}

	public String getResponseMessage() throws IOException {
		return httpsURLConnection.getResponseMessage();
	}
	
}
