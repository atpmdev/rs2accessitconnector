package com.alnt.connector.provisioning.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.log4j.Logger;
import org.json.JSONArray;

import com.alnt.access.common.constants.IProvisoiningConstants;
import com.alnt.access.provisioning.model.IAttrSearchResult;
import com.alnt.access.provisioning.model.IBadgeInformation;
import com.alnt.access.provisioning.model.IProvisioningAttributes;
import com.alnt.access.provisioning.model.IProvisioningResult;
import com.alnt.access.provisioning.model.IProvisioningStatus;
import com.alnt.access.provisioning.model.IRoleAuditInfo;
import com.alnt.access.provisioning.model.ISystemInformation;
import com.alnt.access.provisioning.model.SystemInformation;
import com.alnt.access.provisioning.services.IConnectionInterface;
import com.alnt.connector.constants.RS2AccessItConnectorConstants;
import com.alnt.connector.exception.RS2AccessItConnectorException;
import com.alnt.connector.provisioning.helper.AttributeMetaDataHelper;
import com.alnt.connector.provisioning.helper.RS2AccessItClientHelper;
import com.alnt.connector.provisioning.helper.RS2AccessItURLConnection;
import com.alnt.connector.provisioning.model.AccessLevel;
import com.alnt.connector.provisioning.model.BadgeInformation;
import com.alnt.connector.provisioning.model.Card;
import com.alnt.connector.provisioning.model.CardAccessLevel;
import com.alnt.connector.provisioning.model.CardHolder;
import com.alnt.connector.provisioning.model.CardHolderImage;
import com.alnt.connector.provisioning.model.CardHolderUserColumns;
import com.alnt.connector.provisioning.model.CardholderAccessLevel;
import com.alnt.connector.provisioning.model.ProvisioningResult;
import com.alnt.connector.provisioning.model.RoleAuditInfo;
import com.alnt.connector.provisioning.model.RoleInformation;
import com.alnt.connector.provisioning.model.UserInformation;
import com.alnt.extractionconnector.common.constants.IExtractionConstants;
import com.alnt.extractionconnector.common.constants.IExtractionConstants.USER_ENTITLEMENT_KEY;
import com.alnt.extractionconnector.common.model.IUserInformation;
import com.alnt.extractionconnector.common.service.IExtractionInterface;
import com.alnt.extractionconnector.common.service.ISearchCallback;
import com.alnt.extractionconnector.exception.ExtractorConnectionException;
import com.alnt.extractionconnector.user.model.ExtractorAttributes;
import com.alnt.fabric.component.rolemanagement.search.IRoleInformation;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

/**
 * 
 * @author soori
 *
 */
public class RS2AccessItConnectionInterface implements IConnectionInterface, IExtractionInterface {

	private static final String CLASS_NAME = "com.alnt.connector.provisioning.services.RS2AccessItConnectionInterface";
	private static Logger logger = Logger.getLogger(CLASS_NAME);
	public String _separator = null;
	public String _baseUrl = null;
	public String _apiUserName = null;
	public String _apiPassword = null;
	public String _publicKey = null;
	public String _alertAppDateFormat = null;
	private String externalUserIdAttribute = null;
	private boolean _enableProvisioningWarnings = true;
	private boolean _facilitycodeAssociatedWithBadgeId = false;

	private static final Map<Integer, String> STATUS_MAP;
	static {
		Map<Integer, String> cardStatusMap = new HashMap<Integer, String>();
		cardStatusMap.put(RS2AccessItConnectorConstants.CARD_STATUS_ACTIVE, RS2AccessItConnectorConstants.CARD_STATUS_ACTIVE_STRING);
		cardStatusMap.put(RS2AccessItConnectorConstants.CARD_STATUS_INACTIVE, RS2AccessItConnectorConstants.CARD_STATUS_INACTIVE_STRING);
		cardStatusMap.put(RS2AccessItConnectorConstants.CARD_STATUS_DATEBASED, RS2AccessItConnectorConstants.CARD_STATUS_DATEBASED_STRING);
		STATUS_MAP = Collections.unmodifiableMap(cardStatusMap);
	}


	public RS2AccessItConnectionInterface(Map<String, String> connectionParams) throws RS2AccessItConnectorException {
		try {
			_baseUrl = (String) connectionParams.get(RS2AccessItConnectorConstants.BASE_URL);
			_apiUserName = (String) connectionParams.get(RS2AccessItConnectorConstants.API_USER_NAME);
			_apiPassword = (String) connectionParams.get(RS2AccessItConnectorConstants.API_PASSWORD);
			_publicKey = (String) connectionParams.get(RS2AccessItConnectorConstants.PUBLIC_KEY);
			_alertAppDateFormat = (String) connectionParams.get(RS2AccessItConnectorConstants.ALERT_APP_DATE_FORMAT);
			if (connectionParams.containsKey(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS)) {
				_enableProvisioningWarnings = Boolean.parseBoolean(connectionParams.get(RS2AccessItConnectorConstants.ENABLE_PROVISION_WARNINGS));
			}
			if (connectionParams.containsKey(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID)) {
				_facilitycodeAssociatedWithBadgeId = Boolean.parseBoolean(connectionParams.get(RS2AccessItConnectorConstants.IS_FACILITYCODE_ASSOCIATEDWITH_BADGEID));
			}
			if (connectionParams.containsKey(RS2AccessItConnectorConstants.FACILITYCODE_BADGEID_SEPARATOR)) {
				_separator = connectionParams.get(RS2AccessItConnectorConstants.FACILITYCODE_BADGEID_SEPARATOR);
			} else {
				_separator = RS2AccessItConnectorConstants.DEFAULT_FACILITYCODE_BADGEID_SEPARATOR;
			}
			externalUserIdAttribute = RS2AccessItConnectorConstants.RS2ACCESSIT_USERID;
		} catch (Exception _e) {
			throw new RS2AccessItConnectorException("connection parameters should be not be NULL");
		}
	}

	// RECON OPERATIONS :: BEGIN

	/**
	 * 
	 * soori - 2019-12-06 - 12:41:01 pm
	 * 
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.role.services.IRoleExtractionConnectionInterface#getAllRoles(java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 *
	 */
	@Override
	public void getAllRoles(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " Start of getAllRoles(),  Begin");
			List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
			List<AccessLevel> accessLevels = new ArrayList<AccessLevel>();
			String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_ACCESS_LEVELS);
			RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
			if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						Type listType = new TypeToken<List<AccessLevel>>() {
						}.getType();
						accessLevels = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
			for (AccessLevel accessLevel : accessLevels) {
				String roleId = accessLevel.getAccessLevelID();
				String roleName = accessLevel.getAccessLevelName();
				String roleDesc = accessLevel.getAccessLevelName();
				IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc);
				Map<String, List<String>> memberData = new HashMap<String, List<String>>();
				List<String> values = new ArrayList<String>();
				values.add("" + roleId);
				memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
				getRoleAttribute(options, RS2AccessItConnectorConstants.ATTR_ROLE_ID, roleId, memberData);
				getRoleAttribute(options, RS2AccessItConnectorConstants.ATTR_ROLE_NAME, roleName, memberData);
				roleInformation.setMemberData(memberData);
				rolesList.add(roleInformation);
			}
			callback.processSearchResult(rolesList);
			logger.debug(CLASS_NAME + " getAllRoles(): End of getAllRoles(), ");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getRoles() : execution failed ", e);
		}
	}

	/**
	 * 
	 * soori -10-Dec-2019 - 5:15:47 pm
	 * 
	 * @param lastRunDate
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.role.services.IRoleExtractionConnectionInterface#getIncrementalRoles(java.util.Date, java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 */
	@Override
	public void getIncrementalRoles(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {

			// ?filter=lastmodified gt datetime'2019-12-12T01:30:00'
			// NOTE :: following filter is not working , it is retuning full records
			String lastRunString = parseDateRS2StringFormat(lastRunDate);
			String filterQuery = "?filter=lastmodified gt datetime'" + lastRunString + "'";
			String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_ACCESS_LEVELS).concat(filterQuery);
			// String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_ACCESS_LEVELS);
			logger.debug(CLASS_NAME + " Start of getIncrementalRoles(), Begin");
			List<IRoleInformation> rolesList = new ArrayList<IRoleInformation>();
			List<AccessLevel> accessLevels = new ArrayList<AccessLevel>();
			RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
			if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						Type listType = new TypeToken<List<AccessLevel>>() {
						}.getType();
						accessLevels = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
					}
				}
			}
			for (AccessLevel accessLevel : accessLevels) {
				String roleId = accessLevel.getAccessLevelID();
				String roleName = accessLevel.getAccessLevelName();
				String roleDesc = accessLevel.getAccessLevelName();
				IRoleInformation roleInformation = getRoleInformation(options, roleName, roleDesc);
				Map<String, List<String>> memberData = new HashMap<String, List<String>>();
				List<String> values = new ArrayList<String>();
				values.add("" + roleId);
				memberData.put(IExtractionConstants.TECHNICAL_ROLE_NAME, values);
				getRoleAttribute(options, RS2AccessItConnectorConstants.ATTR_ROLE_ID, roleId, memberData);
				getRoleAttribute(options, RS2AccessItConnectorConstants.ATTR_ROLE_NAME, roleName, memberData);
				roleInformation.setMemberData(memberData);
				rolesList.add(roleInformation);
			}
			callback.processSearchResult(rolesList);
			logger.debug(CLASS_NAME + " getIncrementalRoles(): End  ");
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getIncrementalRoles() : execution failed ", e);
		}

	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:33:13 pm
	 * 
	 * @param options
	 * @param fetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.user.services.IUserExtractionConnectionInterface#getAllUsersWithCallback(java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 *
	 */
	@Override
	public void getAllUsersWithCallback(Map<String, List<ExtractorAttributes>> options, int fetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {
		try {
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): Start of getAllUsersWithCallback method");
			long start = System.currentTimeMillis();
			int totalUsers = 0;
			List<IUserInformation> userInfoList = new ArrayList<IUserInformation>();
			List<CardHolder> cardHolders = null;

			for (char alphabet = 'A'; alphabet <= 'Z'; alphabet++) {
				cardHolders = new ArrayList<CardHolder>();
				// ?includeAllData=true&filter=startswith(lastname, b) or startswith(lastname, B)
				String filterQuery = "?includeAllData=true&filter=startswith(lastname, " + Character.toLowerCase(alphabet) + ") or startswith(lastname, " + alphabet + ")";
				logger.debug(CLASS_NAME + " getAllUsersWithCallback(): Start of recon for Users with Lastname starts with" + alphabet);
				String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat(filterQuery);
				logger.debug(CLASS_NAME + " getAllUsersWithCallback(): endPoint " + endPoint);
				RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
				if (rs2Connection.getResponseCode() != HttpStatus.SC_OK) {
					logger.error(CLASS_NAME + " getAllUsersWithCallback()  Error While fetching Users " + rs2Connection.getResponseMessage());
				} else if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
					String output = bufferedReader.readLine();
					if (output != null) {
						JSONArray restapiresponseArray = new JSONArray(output);
						if (null != restapiresponseArray && restapiresponseArray.length() > 0) {

							Type listType = new TypeToken<List<CardHolder>>() {
							}.getType();
							cardHolders = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
						}
					}
				}
				logger.debug(CLASS_NAME + " getAllUsersWithCallback(): found  " + cardHolders.size() + "  Users with Lastname starts with" + alphabet);
				totalUsers = totalUsers + cardHolders.size();
				if (null != cardHolders && !cardHolders.isEmpty()) {
					for (CardHolder cardholder : cardHolders) {
						IUserInformation userInfo = processExternalUserInfo(cardholder, options, null);
						userInfoList.add(userInfo);
					}
					callback.processSearchResult(userInfoList);
				}
			}
			long end = System.currentTimeMillis();
			float msec = end - start;
			float sec = msec / 1000F;
			float minutes = sec / 60F;
			logger.debug(CLASS_NAME + " getAllUsersWithCallback(): total time taken for  " + totalUsers + "  User Recon   is " + minutes + "  minutes");
		} catch (Exception _ex) {
			logger.error(CLASS_NAME + " getAllUsersWithCallback()  Exception While fetching Users " + _ex.getLocalizedMessage());
		}

	}

	/**
	 * 
	 * soori -10-Dec-2019 - 2:57:41 pm
	 * 
	 * @param lastRunDate
	 * @param options
	 * @param intFetchSize
	 * @param searchCriteria
	 * @param callback
	 * @throws ExtractorConnectionException
	 * @see com.alnt.extractionconnector.user.services.IUserExtractionConnectionInterface#getIncrementalUsersWithCallback(java.util.Date, java.util.Map, int, java.util.Map, com.alnt.extractionconnector.common.service.ISearchCallback)
	 *
	 */
	@Override
	public void getIncrementalUsersWithCallback(Date lastRunDate, Map<String, List<ExtractorAttributes>> options, final int intFetchSize, Map<String, String> searchCriteria, ISearchCallback callback) throws ExtractorConnectionException {

		try {
			String lastRunString = parseDateRS2StringFormat(lastRunDate);
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): Start of getIncrementalUsersWithCallback method");
			long start = System.currentTimeMillis();
			int totalUsers = 0;
			List<IUserInformation> userInfoList = new ArrayList<IUserInformation>();
			List<CardHolder> cardHolders = null;
			Set<String> processedUsers = new HashSet<String>();

			// find all cardholders who are modified from last Run , intern it will get all cards modified since last run.
			for (char alphabet = 'A'; alphabet <= 'Z'; alphabet++) {
				userInfoList = new ArrayList<IUserInformation>();
				cardHolders = new ArrayList<CardHolder>();
				// ?includeAllData=true&filter=lastmodified gt datetime'2019-09-09T09:09:09' and (startswith(lastname, a) or startswith(lastname, A))
				String filterQuery = "?includeAllData=true&filter=lastmodified gt datetime'" + lastRunString + "' and (startswith(lastname, " + Character.toLowerCase(alphabet) + ") or startswith(lastname, " + alphabet + "))";
				logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): Start of recon for Users with Lastname starts with" + alphabet);
				String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat(filterQuery);
				logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): endPoint " + endPoint);
				RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
				if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
					String output = bufferedReader.readLine();
					if (output != null) {
						JSONArray restapiresponseArray = new JSONArray(output);
						if (null != restapiresponseArray && restapiresponseArray.length() > 0) {

							Type listType = new TypeToken<List<CardHolder>>() {
							}.getType();
							cardHolders = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
						}
					}
				}
				logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): found  " + cardHolders.size() + "  Users with Lastname starts with" + alphabet);
				totalUsers = totalUsers + cardHolders.size();
				if (null != cardHolders && !cardHolders.isEmpty()) {
					for (CardHolder cardholder : cardHolders) {
						IUserInformation userInfo = processExternalUserInfo(cardholder, options, lastRunString);
						processedUsers.add(cardholder.getCardholderId());
						userInfoList.add(userInfo);
					}
					callback.processSearchResult(userInfoList);
				}
			}
			// find all cards for those details are not modified , get cards after last modified and check whether that user was processed in not process that user
			List<Card> recentlyModifedCards = getCardByUserAndNumber(null, null, lastRunString);
			if (null != recentlyModifedCards && !recentlyModifedCards.isEmpty()) {
				userInfoList = new ArrayList<IUserInformation>();
				for (Card cardToProcess : recentlyModifedCards) {
					// it means user is not modified but is modified
					if (processedUsers.add(cardToProcess.getCardholderId())) {
						try {
							CardHolder user = getUserByCardHolderId(cardToProcess.getCardholderId());
							IUserInformation userInfo = processExternalUserInfo(user, options, lastRunString);
							userInfoList.add(userInfo);
						} catch (Exception _ex) {
							logger.error(CLASS_NAME + " getIncrementalUsersWithCallback()  error , but continues");
						}
					}
				}
				callback.processSearchResult(userInfoList);
			}

			long end = System.currentTimeMillis();
			float msec = end - start;
			float sec = msec / 1000F;
			float minutes = sec / 60F;
			logger.debug(CLASS_NAME + " getIncrementalUsersWithCallback(): total time taken for  " + totalUsers + "  User Recon   is " + minutes + "  minutes");
		} catch (Exception _ex) {
			logger.error(CLASS_NAME + " getIncrementalUsersWithCallback()  Exception While fetching Users " + _ex.getLocalizedMessage());
		}

	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:28:29 pm
	 * 
	 * @param cardHolder
	 * @param options
	 * @return IUserInformation
	 *
	 */
	private IUserInformation processExternalUserInfo(CardHolder cardHolder, Map<String, List<ExtractorAttributes>> options, String lastRunAt) {
		IUserInformation userInformation = new UserInformation();
		Map<String, Map<String, List<Map<String, Object>>>> allEntitlements = new HashMap<String, Map<String, List<Map<String, Object>>>>();
		Map<String, Object> userAttr = new HashMap<String, Object>();
		if (null != cardHolder) {
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_ID, cardHolder.getCardholderId(), options, userAttr, "User");
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_FN, cardHolder.getFirstName(), options, userAttr, "User");
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_LN, cardHolder.getLastName(), options, userAttr, "User");
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_MEMEBER_OF_ALL_SITES, cardHolder.getMemberOfAllSites(), options, userAttr, "User");
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_STATTUS, cardHolder.getCardholderStatus(), options, userAttr, "User");
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_COMPANY_ID, cardHolder.getCompanyId(), options, userAttr, "User");
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_MIDDLE_INITIAL, cardHolder.getMiddleInitial(), options, userAttr, "User");
			// TODO : format Dates to Alert Date Format if they are not null, check with adept ExtractionAttribute format ??
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE, cardHolder.getCardholderActiveDate(), options, userAttr, "User");
			userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE, cardHolder.getCardholderExpireDate(), options, userAttr, "User");
			if (null != cardHolder.getUserColumns()) {
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_DEPARTMENT, cardHolder.getUserColumns().getDepartment(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT1, cardHolder.getUserColumns().getUserText1(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT2, cardHolder.getUserColumns().getUserText2(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT3, cardHolder.getUserColumns().getUserText3(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT4, cardHolder.getUserColumns().getUserText4(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT5, cardHolder.getUserColumns().getUserText5(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT6, cardHolder.getUserColumns().getUserText6(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT7, cardHolder.getUserColumns().getUserText7(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT8, cardHolder.getUserColumns().getUserText8(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT9, cardHolder.getUserColumns().getUserText9(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT10, cardHolder.getUserColumns().getUserText10(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT11, cardHolder.getUserColumns().getUserText11(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT12, cardHolder.getUserColumns().getUserText12(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT13, cardHolder.getUserColumns().getUserText13(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT14, cardHolder.getUserColumns().getUserText14(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT15, cardHolder.getUserColumns().getUserText15(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT16, cardHolder.getUserColumns().getUserText16(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT17, cardHolder.getUserColumns().getUserText17(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT18, cardHolder.getUserColumns().getUserText18(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT19, cardHolder.getUserColumns().getUserText19(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERTEXT20, cardHolder.getUserColumns().getUserText20(), options, userAttr, "User");
				// TODO : format Dates to Alert Date Format if they are not null, check with adept ExtractionAttribute format ??
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERDATE1, cardHolder.getUserColumns().getUserDate1(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERDATE2, cardHolder.getUserColumns().getUserDate2(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERDATE3, cardHolder.getUserColumns().getUserDate3(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERDATE4, cardHolder.getUserColumns().getUserDate4(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERDATE5, cardHolder.getUserColumns().getUserDate5(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC1, cardHolder.getUserColumns().getUserNumeric1(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC2, cardHolder.getUserColumns().getUserNumeric2(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC3, cardHolder.getUserColumns().getUserNumeric3(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC4, cardHolder.getUserColumns().getUserNumeric4(), options, userAttr, "User");
				userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_USERNUMERIC5, cardHolder.getUserColumns().getUserNumeric5(), options, userAttr, "User");
			}
			if (null != cardHolder.getImages() && !cardHolder.getImages().isEmpty()) {
				for (CardHolderImage userImage : cardHolder.getImages()) {
					// TODO:: extend filter if you have to process all the image types
					if (userImage.getImageType() == RS2AccessItConnectorConstants.USER_IMAGE_TYPE_PHOTO) {
						userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_IMAGETYPE, userImage.getImageType(), options, userAttr, "User");
						userAttr = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA, userImage.getImageData(), options, userAttr, "User");
					}
				}
			}
			List<Map<String, Object>> rolesList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> badgesList = new ArrayList<Map<String, Object>>();
			List<Card> userBadges = null;

			try {
				if (null != lastRunAt) {
					userBadges = getCardByUserAndNumber(cardHolder.getCardholderId(), null, lastRunAt);
				} else {
					userBadges = getCardByUserAndNumber(cardHolder.getCardholderId(), null, null);
				}
			} catch (Exception e) {
			}

			if (userBadges != null && !userBadges.isEmpty()) {
				logger.debug(CLASS_NAME + " processExternalUserInfo(): enter processing user badge info");
				for (Card userBadge : userBadges) {
					logger.debug(CLASS_NAME + " processExternalUserInfo(): start processing badge information");
					Map<String, Object> processedDataMap = new HashMap<String, Object>();
					if (_facilitycodeAssociatedWithBadgeId) {
						processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, userBadge.getFacilityCode() + _separator + userBadge.getCardNumber(), options, processedDataMap, "Badge");
					} else {
						processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, userBadge.getCardNumber(), options, processedDataMap, "Badge");
					}
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, userBadge.getFacilityCode(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_NUMBER, userBadge.getPinNumber(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_PIN_EXCEMPT, userBadge.getPinExempt(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_APBEXCEMPT, userBadge.getApbExempt(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_USE_EXTENDED_ACCESS_TIMES, userBadge.getUseExtendedAccessTimes(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_STATUS, userBadge.getCardStatus(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_USER_LEVEL, userBadge.getUserLevel(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_USER_CUSTOM_REPORTING, userBadge.getUseCustomReporting(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUE_LEVEL, userBadge.getIssueLevel(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_DEACTIVATE_EXCEMPT, userBadge.getDeactivateExempt(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_USE_COUNT, userBadge.getUseCount(), options, processedDataMap, "Badge");
					// optional
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_USERTYPE, userBadge.getIpLocksetUserType(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_MODE, userBadge.getIpLocksetAccessMode(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_IP_LOCKSET_ACCESS_ALWAYS, userBadge.getIpLocksetAccessAlways(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_TYPE, userBadge.getBadgeType(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_ISSUED_BY, userBadge.getIssuedBy(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_NO_OF_TIMES_PRINTED, userBadge.getNumberOfTimesPrinted(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_BY_USER, userBadge.getCreatedByUser(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_LAST_MODIFIED_BY_USER, userBadge.getLastModifiedByUser(), options, processedDataMap, "Badge");
					// TODO :: convert to ALERT DATE format, check with adept ExtractionAttribute format ??
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE, userBadge.getActiveDate(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE, userBadge.getExpireDate(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_LAST_MODIFIED, userBadge.getLastModified(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_CREATED_DATE, userBadge.getDateCreated(), options, processedDataMap, "Badge");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_BADGE_DATE_ISSUED, userBadge.getDateIssued(), options, processedDataMap, "Badge");
					badgesList.add(processedDataMap);
					Card cardDetails = null;
					try {
						cardDetails = getCardByCardId(userBadge.getCardId());
					} catch (Exception e) {

					}
					if (null != cardDetails) {
						if (null != cardDetails.getCardAccessLevels() && !cardDetails.getCardAccessLevels().isEmpty()) {
							for (CardAccessLevel userBadgeRole : cardDetails.getCardAccessLevels()) {
								logger.debug(CLASS_NAME + " processExternalUserInfo(): start processing user role information");
								Map<String, Object> processedBadgeRoleDataMap = new HashMap<String, Object>();
								processedBadgeRoleDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_ROLE_ID, userBadgeRole.getAccessLevelID(), options, processedBadgeRoleDataMap, "Role");
								// TODO dates , check with adept ExtractionAttribute format ??
								processedBadgeRoleDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_ROLE_ACTIVE_DATE, userBadgeRole.getActivateDate(), options, processedBadgeRoleDataMap, "Role");
								processedBadgeRoleDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_ROLE_EXPITY_DATE, userBadgeRole.getDeactivateDate(), options, processedBadgeRoleDataMap, "Role");
								rolesList.add(processedDataMap);
								logger.debug(CLASS_NAME + " processExternalUserInfo(): finished user role information processing");
							}
						}
					}
					logger.debug(CLASS_NAME + " processExternalUserInfo(): finished badge information processing");
				}
				logger.debug(CLASS_NAME + " processExternalUserInfo(): finished processing user badge info");
			}
			if (null != cardHolder.getCardholderAccessLevels() && !cardHolder.getCardholderAccessLevels().isEmpty()) {
				for (CardholderAccessLevel userRole : cardHolder.getCardholderAccessLevels()) {
					logger.debug(CLASS_NAME + " processExternalUserInfo(): start processing user role information");
					Map<String, Object> processedDataMap = new HashMap<String, Object>();
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_ROLE_ID, userRole.getAccessLevelID(), options, processedDataMap, "Role");
					// TODO dates , check with adept ExtractionAttribute format ??
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_ROLE_ACTIVE_DATE, userRole.getActivateDate(), options, processedDataMap, "Role");
					processedDataMap = convertFormat(RS2AccessItConnectorConstants.ATTR_USER_ROLE_EXPITY_DATE, userRole.getDeactivateDate(), options, processedDataMap, "Role");
					rolesList.add(processedDataMap);
					logger.debug(CLASS_NAME + " processExternalUserInfo(): finished user role information processing");
				}
			}
			if (rolesList != null && rolesList.size() > 0) {
				Map<String, List<Map<String, Object>>> entitlement = new HashMap<String, List<Map<String, Object>>>();
				entitlement.put(USER_ENTITLEMENT_KEY.ROLES.toString(), rolesList);
				if (userAttr.containsKey(RS2AccessItConnectorConstants.ATTR_USER_ID) && userAttr.get(RS2AccessItConnectorConstants.ATTR_USER_ID) != null) {
					allEntitlements.put(userAttr.get(RS2AccessItConnectorConstants.ATTR_USER_ID).toString(), entitlement);
				} else {
					logger.debug("Alert attribute userId is not mapped to any of external system's attribute or mapped external attriute is null");
				}
			}
			if (badgesList != null && badgesList.size() > 0) {
				Map<String, List<Map<String, Object>>> entitlement = new HashMap<String, List<Map<String, Object>>>();
				entitlement.put(USER_ENTITLEMENT_KEY.BADGES.toString(), badgesList);
				if (userAttr.containsKey(RS2AccessItConnectorConstants.ATTR_USER_ID) && userAttr.get(RS2AccessItConnectorConstants.ATTR_USER_ID) != null) {
					allEntitlements.put(userAttr.get(RS2AccessItConnectorConstants.ATTR_USER_ID).toString(), entitlement);
				} else {
					logger.debug("Alert attribute userId is not mapped to any of external system's attribute or mapped external attriute is null");
				}
			}
			userInformation.setEntitlements(allEntitlements);
			userInformation.setUserDetails(userAttr);
		}
		return userInformation;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:28:18 pm
	 * 
	 * @param fieldName
	 * @param fieldValue
	 * @param options
	 * @param userAttr
	 * @param type
	 * @return
	 *
	 */
	private Map<String, Object> convertFormat(String fieldName, Object fieldValue, Map<String, List<ExtractorAttributes>> options, Map<String, Object> userAttr, String type) {
		try {
			if (options != null && options.size() > 0 && options.containsKey(fieldName) && null != fieldValue ) {
				List<ExtractorAttributes> extractorAttributesList = options.get(fieldName);
				if (extractorAttributesList != null && extractorAttributesList.size() > 0) {
					for (ExtractorAttributes extractorAttribute : extractorAttributesList) {
						boolean typeMatch = (type.equals("User") && extractorAttribute.isUserAttr()) || (type.equals("Badge") && extractorAttribute.isBadgeAttr()) || (type.equals("Role") && extractorAttribute.isRoleAttr());
						if (extractorAttribute != null && typeMatch) {
							logger.debug(CLASS_NAME + " processOutMap() : setting value for: " + extractorAttribute.getAttributeName());
							if (extractorAttribute.getFieldType() == IExtractionConstants.TYPE.DATE) {
								// converting connector date to alert date format String some dates has .00 ,some has .000 as milliseconds
								if (fieldValue.toString().contains(".")) {
									String dateString = fieldValue.toString();
									for (int j = dateString.length(); j < 23; j++) {
										dateString = dateString.concat("0");
									}
									userAttr.put(extractorAttribute.getAttributeName(), parseDateStringToDateString(dateString, RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT_SECONDS, _alertAppDateFormat));
								} else {
									userAttr.put(extractorAttribute.getAttributeName(), parseDateStringToDateString(fieldValue.toString(), RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT, _alertAppDateFormat));
								}
							} else if (extractorAttribute.getFieldType() == IExtractionConstants.TYPE.IMAGE) {
								byte[] image =  Base64.getEncoder().encode(fieldValue.toString().getBytes());
								if(image != null) {
									userAttr.put(extractorAttribute.getAttributeName(),image);
								}
							} else {
								userAttr.put(extractorAttribute.getAttributeName(), fieldValue.toString());
							}
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " convertFormat() : Error setting the value for " + fieldName, e);
		}
		return userAttr;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:34:22 pm
	 * 
	 * @param options
	 * @param roleId
	 * @param roleDesc
	 * @return IRoleInformation
	 *
	 */
	private IRoleInformation getRoleInformation(Map<String, List<ExtractorAttributes>> options, String roleId, String roleDesc) {
		IRoleInformation roleInformation = new RoleInformation();
		logger.trace(CLASS_NAME + " getRoleInformation(): start processing role name " + roleId);
		roleInformation.setRoleType(IExtractionConstants.ROLE_TYPE_SINGLE);
		roleInformation.setName(roleDesc);
		roleInformation.setDescription(roleDesc);
		roleInformation.setValidFrom(null);
		roleInformation.setValidTo(null);
		logger.trace(CLASS_NAME + " getRoleInformation(): finish processing role name " + roleId);
		return roleInformation;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:06:34 pm void
	 * 
	 * @param options
	 * @param attributeName
	 * @param attributeValue
	 * @param memberData
	 *
	 */
	private void getRoleAttribute(Map<String, List<ExtractorAttributes>> options, String attributeName, String attributeValue, Map<String, List<String>> memberData) {
		if (options.containsKey(attributeName)) {
			List<String> values = new ArrayList<String>();
			List<ExtractorAttributes> companyAttr = options.get(attributeName);
			for (ExtractorAttributes extractorAttributes : companyAttr) {
				if (extractorAttributes.isRoleAttr()) {
					values = new ArrayList<String>();
					values.add("" + attributeValue);
					memberData.put(extractorAttributes.getAttributeName(), values);
				}
			}
		}
	}

	@Override
	public List<IUserInformation> getIncrementalUsers(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void getUsers(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void invokeUsers(Map arg0, List arg1) throws ExtractorConnectionException {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getAllRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getAllRoles(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<com.alnt.extractionconnector.common.model.IRoleInformation> getIncrementalRoles(Date arg0, Map<String, List<ExtractorAttributes>> arg1, int arg2, int arg3, Map<String, String> arg4) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRoles(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getRolesForUser(String arg0) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public void searchRoles(String arg0, ISearchCallback arg1) throws ExtractorConnectionException {
	}

	@Override
	public boolean supportsProvisioning() {
		return false;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public Map getAllUsers(Map arg0, List arg1) throws ExtractorConnectionException {
		return null;
	}

	@Override
	public List<IUserInformation> getAllUsers(Map<String, List<ExtractorAttributes>> arg0, int arg1, int arg2, Map<String, String> arg3) throws ExtractorConnectionException {
		return null;
	}
	// RECON OPERATIONS :: END

	// PROVISIONING OPERATIONS :: BEGIN
	/**
	 * 
	 * soori 2019-12-02 - 5:36:27 pm
	 * 
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#testConnection()
	 *
	 *
	 */
	@Override
	public boolean testConnection() throws Exception {
		logger.info(CLASS_NAME + " testConnection(): Start of testConnection method");
		try {
			String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_BADGETYPES);
			RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
			if (rs2Connection.getResponseCode() != 200) {
				logger.debug("Test connection failed , " + rs2Connection.getResponseMessage());
				return false;
			}
		} catch (Exception _ex) {
			logger.debug("Test connection failed , " + _ex.getLocalizedMessage());
			return false;
		}
		return true;
	}

	/**
	 * 
	 * soori - 9:57:14 pm
	 * 
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#getAttributes()
	 */
	@Override
	public List<IProvisioningAttributes> getAttributes() throws Exception {
		return AttributeMetaDataHelper.getAttributes();
	}

	/**
	 * 
	 * soori -10-Dec-2019 - 5:19:17 pm
	 * 
	 * @param params
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#getAttributes(java.util.Map)
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getAttributes(Map params) throws Exception {
		// TODO :: review pending Read the customFieldName for which Data needs to be fetched
		String customFieldName = (String) params.get("API_NAME");
		if ((null != customFieldName) && (!customFieldName.isEmpty())) {
			return AttributeMetaDataHelper.getAttributes(customFieldName);
		} else {
			logger.debug(CLASS_NAME + " " + "getAttributes(Map params)" + " API_NAME name is not provided. So returning!! ");
			return new ArrayList<IAttrSearchResult>();
		}
	}

	/**
	 * 
	 * soori - 8:23:42 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserProvisioned(java.lang.String)
	 */
	@Override
	public ISystemInformation isUserProvisioned(String userId) throws Exception {
		logger.debug(CLASS_NAME + " isUserProvisioned(): Checking isUserProvisioned  with  CardHolder Id  : " + userId);
		ISystemInformation systemInformation = new SystemInformation(false);
		try {
			CardHolder user = getUserByCardHolderId(userId);
			if (null != user) {
				systemInformation.setProvisioned(true);
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " isUserProvisioned(): Error while call to external system", e);
			systemInformation.setProvisioned(false);
		}
		return systemInformation;
	}

	/**
	 * 
	 * soori - 9:55:29 pm
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#isUserLocked(java.lang.String)
	 */
	@Override
	public boolean isUserLocked(String userId) throws Exception {
		CardHolder user = getUserByCardHolderId(userId);
		if (null != user && user.getCardholderStatus() == RS2AccessItConnectorConstants.CARDHOLDER_STATUS_INACTIVE) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * soori - 10:33:36 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#create(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public IProvisioningResult create(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " create(): Start of Create Method.");
		IProvisioningResult provResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "User is not provisioned !!!");
		boolean userCreated = true;
		OutputStream outputStream = null;
		try {
			logger.debug(CLASS_NAME + " create(): parameters .  :: "+ parameters);
			CardHolder cardHolder = prepareCardHolder(parameters);
			String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS);
			RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _publicKey);
			Gson gson = new Gson();
			String gsonString = gson.toJson(cardHolder);
			logger.debug(CLASS_NAME + "   create()   request payload  :::  " + gsonString);
			outputStream = rs2Connection.getOutputStream();
			outputStream.write(gsonString.getBytes());
			outputStream.flush();
			if (rs2Connection.getResponseCode() != HttpStatus.SC_OK) {
				logger.error(CLASS_NAME + "   create()   " + rs2Connection.getResponseMessage());
				provResult.setMsgDesc(rs2Connection.getResponseMessage());
				return provResult;
			} else if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						List<CardHolder> cardHolders;
						Type listType = new TypeToken<List<CardHolder>>() {
						}.getType();
						cardHolders = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
						CardHolder cardHolderCreated = cardHolders.get(0);
						if (!cardHolderCreated.getCardholderId().isEmpty()) {
							provResult.setUserId(cardHolderCreated.getCardholderId());
							provResult.setUserCreated(userCreated);
							provResult.setMsgCode(IProvisoiningConstants.CREATE_USER_SUCCESS);
							provResult.setMsgDesc("User provisioned successfully");
							provResult.setProvFailed(false);
							parameters.put(externalUserIdAttribute, cardHolderCreated.getCardholderId());
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + "create()", e);
			provResult.setMsgDesc(e.getMessage());
			return provResult;
		}
		logger.debug(CLASS_NAME + " create(): End of Create Method.");
		return provResult;
	}

	/**
	 * 
	 * soori -10-Dec-2019 - 5:17:17 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#update(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 */
	@Override
	@SuppressWarnings("rawtypes")
	public IProvisioningResult update(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " update(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "update user failed in target system!");
		OutputStream outputStream = null;
		try {
			logger.debug(CLASS_NAME + " update() parameters passed!!  :: " + parameters);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " update() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToUpdate = parameters.get(externalUserIdAttribute).toString();
				CardHolder cardHolder = getUserByCardHolderId(userIdToUpdate);

				if (null != cardHolder) {
					CardHolder cardHolderInRequest = prepareCardHolder(parameters);
					// XXX :: BeanUtils.copyProperties(cardHolder, cardHolderInRequest); //clone using reflection
					cardHolder.setFirstName(cardHolderInRequest.getFirstName());
					cardHolder.setLastName(cardHolderInRequest.getLastName());
					if (null != cardHolderInRequest.getImages() && !cardHolderInRequest.getImages().isEmpty()) {
						// TODO :: too much math required if we have to update only specific image if connector is used for multiple images
						// if card holder has no images , has same type of image , different image type , and bothSSSSS
						cardHolder.setImages(cardHolderInRequest.getImages());
					}
					String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat("/").concat(userIdToUpdate);
					RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
					Gson gson = new Gson();
					String gsonString = gson.toJson(cardHolder);
					logger.debug(CLASS_NAME + "   update()   request payload  :::  " + gsonString);
					outputStream = rs2Connection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " update() user does not exist / already updated in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " update() user does not exist / already updated , but warnings are disabled , returning as updated successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " update()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "Error during update the user");
		}
		logger.info(CLASS_NAME + " update(): End of update method");
		return provisioningResult;

	}

	/**
	 * 
	 * 
	 * This method is to delete the user account in external system.
	 * 
	 * soori - 2:16:34 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#deleteAccount(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deleteAccount(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " deleteAccount(): Start of update method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "Error while deleting User in external system");
		ISystemInformation systemInformation = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " deleteAccount() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIDtoDelete = parameters.get(externalUserIdAttribute).toString();
				systemInformation = isUserProvisioned(userIDtoDelete);
				if (systemInformation != null && systemInformation.isProvisioned()) {
					String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat("/").concat(userIDtoDelete);
					RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_DELETE, _apiUserName, _apiPassword, _publicKey);
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						logger.debug(CLASS_NAME + " deleteAccount() user deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " deleteAccount() user does not exist , but warnings are disabled , returning as deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " deleteAccount(): Error during update the user ", e);
			logger.error(CLASS_NAME + " deleteAccount()", e);
		}
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:54:34 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#lock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult lock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {

		logger.info(CLASS_NAME + " lock(): Start of lock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "lock user failed in target system!");
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " lock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToLock = parameters.get(externalUserIdAttribute).toString();
				CardHolder cardHolder = getUserByCardHolderId(userIdToLock);
				if (null != cardHolder && cardHolder.getCardholderStatus() == RS2AccessItConnectorConstants.CARDHOLDER_STATUS_ACTIVE) {

					String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat("/").concat(userIdToLock);
					RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
					Gson gson = new Gson();
					cardHolder.setCardholderStatus(RS2AccessItConnectorConstants.CARDHOLDER_STATUS_INACTIVE);
					String gsonString = gson.toJson(cardHolder);
					logger.debug(CLASS_NAME + "   lock()   request payload  :::  " + gsonString);
					outputStream = rs2Connection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
						String output = bufferedReader.readLine();
						if (output != null) {
							JSONArray restapiresponseArray = new JSONArray(output);
							if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
								List<CardHolder> cardHolders;
								Type listType = new TypeToken<List<CardHolder>>() {
								}.getType();
								cardHolders = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
								CardHolder updatedCardHolder = cardHolders.get(0);
								if (updatedCardHolder.getCardholderStatus() == RS2AccessItConnectorConstants.CARDHOLDER_STATUS_INACTIVE) {
									provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, null);
								}
							}
						}
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " lock() user does not exist / already locked in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " lock() user does not exist / already locked , but warnings are disabled , returning as locked successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, null);
					}

				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " lock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "Error during lock the user");
		}
		logger.info(CLASS_NAME + " lock(): End of lock method");
		return provisioningResult;

	}

	/**
	 * 
	 * soori - 4:43:19 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#unlock(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult unlock(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " unlock(): Start of unlock method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "unlock user failed in target system!");
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " unlock() userId does not passed!!");
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "userId does not passed!!");
			} else {
				String userIdToUnlock = parameters.get(externalUserIdAttribute).toString();
				CardHolder cardHolder = getUserByCardHolderId(userIdToUnlock);
				if (null != cardHolder && cardHolder.getCardholderStatus() == RS2AccessItConnectorConstants.CARDHOLDER_STATUS_INACTIVE) {

					String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat("/").concat(userIdToUnlock);
					RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
					Gson gson = new Gson();
					cardHolder.setCardholderStatus(RS2AccessItConnectorConstants.CARDHOLDER_STATUS_ACTIVE);
					String gsonString = gson.toJson(cardHolder);
					logger.debug(CLASS_NAME + "   unlock()   request payload  :::  " + gsonString);
					outputStream = rs2Connection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
						String output = bufferedReader.readLine();
						if (output != null) {
							JSONArray restapiresponseArray = new JSONArray(output);
							if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
								List<CardHolder> cardHolders;
								Type listType = new TypeToken<List<CardHolder>>() {
								}.getType();
								cardHolders = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
								CardHolder updatedCardHolder = cardHolders.get(0);
								if (updatedCardHolder.getCardholderStatus() == RS2AccessItConnectorConstants.CARDHOLDER_STATUS_ACTIVE) {
									provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, null);
								}
							}
						}
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " unlock() user does not exist / already unlocked in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "User does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " unlock() user does not exist / already unlocked , but warnings are disabled , returning as unlocked successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, null);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " unlock()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "Error during unlock the user");
		}
		logger.info(CLASS_NAME + " unlock(): End of lock method");
		return provisioningResult;
	}

	/**
	 * 
	 * it required to pass alert date format in connection parameters.it should have time component the value should be same format , if not need to change parameter value in systems
	 * 
	 * 
	 * soori - 5:17:13 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param action
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#delimitUser(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.lang.String, java.util.Map)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult delimitUser(Long requestNumber, List roles, Map parameters, List requestDetails, String action, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " delimitUser(): Start of delimitUser method");
		OutputStream outputStream = null;

		String failureCode = null, successCode = null;
		successCode = action.equals(IProvisoiningConstants.PROV_ACTION_DELIMIT_USER) ? IProvisoiningConstants.DELIMIT_USER_SUCCESS : IProvisoiningConstants.CHANGE_VALIDITY_DATES_SUCCESS;
		failureCode = action.equals(IProvisoiningConstants.PROV_ACTION_DELIMIT_USER) ? IProvisoiningConstants.DELIMIT_USER_FAILURE : IProvisoiningConstants.CHANGE_VALIDITY_DATES_FAILURE;

		IProvisioningResult provisioningResult = prepareProvisioningResult(action, failureCode, true, " unsuccessfull !!");
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " delimitUser() userId does not passed!!");
				return prepareProvisioningResult(action, failureCode, true, "userId does not passed!!");
			}
			if (parameters.get(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE) == null) {
				logger.debug(CLASS_NAME + " delimitUser() userexpirydate is not passed!!");
				return prepareProvisioningResult(action, failureCode, true, "user expirydate does not passed!!");
			}
			if (action.equals(IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES) && parameters.get(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE) == null) {
				logger.debug(CLASS_NAME + " delimitUser() userstartdate is not passed for change validity dates action!!");
				return prepareProvisioningResult(action, failureCode, true, "user startdate does not passed for change validity dates action!!");
			}
			String userIdToUpdate = parameters.get(externalUserIdAttribute).toString();
			CardHolder cardHolder = getUserByCardHolderId(userIdToUpdate);
			if (null != cardHolder) {
				String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat("/").concat(userIdToUpdate);
				RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
				Gson gson = new Gson();
				cardHolder.setCardholderExpireDate(parseDateStringToDateString(parameters.get(RS2AccessItConnectorConstants.ATTR_USER_EXPIRY_DATE).toString(), _alertAppDateFormat, RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT));
				if (action.equals(IProvisoiningConstants.PROV_ACTION_CHANGE_VALIDITY_DATES)) {
					cardHolder.setCardholderActiveDate(parseDateStringToDateString(parameters.get(RS2AccessItConnectorConstants.ATTR_USER_ACTIVE_DATE).toString(), _alertAppDateFormat, RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT));
				}
				String gsonString = gson.toJson(cardHolder);
				logger.debug(CLASS_NAME + "   delimitUser()   request payload  :::  " + gsonString);
				outputStream = rs2Connection.getOutputStream();
				outputStream.write(gsonString.getBytes());
				outputStream.flush();
				if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
					provisioningResult = prepareProvisioningResult(action, successCode, false, null);
				}
			} else {
				if (_enableProvisioningWarnings) {
					logger.debug(CLASS_NAME + " delimitUser() user does not exist ");
					provisioningResult = prepareProvisioningResult(action, failureCode, true, "User does not exist in external system");
				} else {
					logger.debug(CLASS_NAME + " delimitUser() user does not exist , but warnings are disabled , returning as delimited successfully in the system");
					provisioningResult = prepareProvisioningResult(action, successCode, false, null);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " delimitUser(): Error during delimitUser the user ", e);
			logger.error(CLASS_NAME + " delimitUser()", e);
			provisioningResult = prepareProvisioningResult(action, failureCode, true, "Error during delimitUser user");
		}
		logger.info(CLASS_NAME + " delimitUser(): End of delimitUser method");
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-06 - 12:20:15 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#activateBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult activateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " activateBadge(): Start of activateBadge method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "Error activating a user badge: ");
		IBadgeInformation badgeInfo = new BadgeInformation();
		List<Card> cards = null;
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " activateBadge() userId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "UserId does not passed!!");
			}
			if (parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER) == null) {
				logger.debug(CLASS_NAME + " activateBadge() badgeId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "badgeId does not passed!!");
			} else {
				String userId = parameters.get(externalUserIdAttribute).toString();
				String cardNumber = "";
				if (_facilitycodeAssociatedWithBadgeId) {
					cardNumber = getCardNumberFromComboCode(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString());
				} else {
					cardNumber = parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString();
				}
				cards = getCardByUserAndNumber(userId, cardNumber, null);
				if (null != cards && !cards.isEmpty()) {
					logger.debug(CLASS_NAME + " activateBadge() user badge provisioned in the system, proceed to deactivate badge");
					Card cardToActivate = cards.get(0);
					if (cardToActivate.getCardStatus() == RS2AccessItConnectorConstants.CARD_STATUS_ACTIVE) {
						logger.debug(CLASS_NAME + " activateBadge() badge already  Active in the system");
						if (_enableProvisioningWarnings) {
							logger.debug(CLASS_NAME + " activateBadge() badge already  Active in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "badge already  Active in the system");
						} else {
							logger.debug(CLASS_NAME + " activateBadge() badge already  Active in the system , but warnings are disabled , returning as activated successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, null);
							provisioningResult.setBadgeInformation(badgeInfo);
						}
					} else {
						badgeInfo = processCardToBadgeInformation(cards.get(0));
						String cardIdtoActivate = cardToActivate.getCardId();
						String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS).concat("/").concat(cardIdtoActivate);
						RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
						Gson gson = new Gson();
						cardToActivate.setCardStatus(RS2AccessItConnectorConstants.CARD_STATUS_ACTIVE);
						String gsonString = gson.toJson(cardToActivate);
						logger.debug(CLASS_NAME + "   activateBadge()   request payload  :::  " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
							logger.debug(CLASS_NAME + " activateBadge() badge activated successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, "badge activated successfully in the system");
							provisioningResult.setBadgeInformation(badgeInfo);
						}
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " activateBadge() badge does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "badge does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " activateBadge() badge does not exist , but warnings are disabled , returning as activated successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_SUCCESS, false, null);
						provisioningResult.setBadgeInformation(badgeInfo);
					}

				}

			}
		} catch (

		Exception e) {
			logger.error(CLASS_NAME + " activateBadge(): Error during activating user badge", e);
			logger.error(CLASS_NAME + " activateBadge()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_UNLOCK_USER, IProvisoiningConstants.UNLOCK_USER_FAILURE, true, "Error during activating a user badge");
			provisioningResult.setBadgeInformation(badgeInfo);
		}
		logger.info(CLASS_NAME + " activateBadge(): End of activateBadge method");
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-06 - 12:42:46 am
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#deActivateBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult deActivateBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " deActivateBadge(): Start of deActivateBadge method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "Error deactivating a user badge: ");
		IBadgeInformation badgeInfo = new BadgeInformation();
		List<Card> cards = null;
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " deActivateBadge() userId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "UserId does not passed!!");
			}
			if (parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER) == null) {
				logger.debug(CLASS_NAME + " deActivateBadge() badgeId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "badgeId does not passed!!");
			} else {
				String userId = parameters.get(externalUserIdAttribute).toString();
				String cardNumber = "";
				if (_facilitycodeAssociatedWithBadgeId) {
					cardNumber = getCardNumberFromComboCode(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString());
				} else {
					cardNumber = parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString();
				}
				cards = getCardByUserAndNumber(userId, cardNumber, null);
				if (null != cards && !cards.isEmpty()) {
					logger.debug(CLASS_NAME + " deActivateBadge() user badge provisioned in the system, proceed to deactivate badge");
					Card cardToDeactivate = cards.get(0);
					if (cardToDeactivate.getCardStatus() == RS2AccessItConnectorConstants.CARD_STATUS_INACTIVE) {
						logger.debug(CLASS_NAME + " deActivateBadge() badge already  Inactive in the system");
						if (_enableProvisioningWarnings) {
							logger.debug(CLASS_NAME + " deActivateBadge() badge already  Inactive in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "badge already  Inactive in the system");
						} else {
							logger.debug(CLASS_NAME + " deActivateBadge() badge already  Inactive in the system , but warnings are disabled , returning as Inactive successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, null);
							provisioningResult.setBadgeInformation(badgeInfo);
						}
					} else {
						badgeInfo = processCardToBadgeInformation(cards.get(0));
						String cardIdtoDeactivate = cardToDeactivate.getCardId();
						String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS).concat("/").concat(cardIdtoDeactivate);
						RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
						Gson gson = new Gson();
						cardToDeactivate.setCardStatus(RS2AccessItConnectorConstants.CARD_STATUS_INACTIVE);
						String gsonString = gson.toJson(cardToDeactivate);
						logger.debug(CLASS_NAME + "   deActivateBadge()   request payload  :::  " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
							logger.debug(CLASS_NAME + " deActivateBadge() badge deactivated successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, "badge deactivated successfully in the system");
							provisioningResult.setBadgeInformation(badgeInfo);
						}
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " deActivateBadge() badge does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "badge does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " deActivateBadge() badge does not exist , but warnings are disabled , returning as deactivated successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_SUCCESS, false, null);
						provisioningResult.setBadgeInformation(badgeInfo);
					}

				}

			}
		} catch (

		Exception e) {
			logger.error(CLASS_NAME + " deActivateBadge(): Error during activating user badge", e);
			logger.error(CLASS_NAME + " deActivateBadge()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_LOCK_USER, IProvisoiningConstants.LOCK_USER_FAILURE, true, "Error during deactivating a user badge");
			provisioningResult.setBadgeInformation(badgeInfo);
		}
		logger.info(CLASS_NAME + " deActivateBadge(): End of deActivateBadge method");
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-03 - 8:22:08 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#addBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IProvisioningResult addBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " addBadge(): Start of addBadge method");
		IProvisioningResult provisioningResult = null;
		IBadgeInformation badgeInfo = new BadgeInformation();
		OutputStream outputStream = null;
		try {
			logger.debug(CLASS_NAME + " addBadge() :: parameters from  request  :: " + parameters);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " addBadge() userId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "UserId does not passed!!");
			}
			if (_facilitycodeAssociatedWithBadgeId && parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER) != null) {
				parameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_FACILITY_CODE, getFacilityCodeFromComboCode(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString()));
				parameters.put(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER, getCardNumberFromComboCode(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString()));
			}
			Card cardToCreate = prepareCard(parameters);
			if (!validateMandatoryCardData(cardToCreate)) {
				logger.debug(CLASS_NAME + " addBadge() all mandatory data to provision badge is not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "Mandatory data does not passed!!");
			}
			validateAndFormatOptionalData(cardToCreate);
			String userIdToChangeAccess = parameters.get(externalUserIdAttribute).toString();
			CardHolder cardHolder = getUserByCardHolderId(userIdToChangeAccess);
			if (null != cardHolder) {
				String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS);
				RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_POST, _apiUserName, _apiPassword, _publicKey);
				Gson gson = new Gson();
				String gsonString = gson.toJson(cardToCreate);
				logger.debug(CLASS_NAME + "   addBadge()   request payload  :::  " + gsonString);
				outputStream = rs2Connection.getOutputStream();
				outputStream.write(gsonString.getBytes());
				outputStream.flush();
				if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {

					BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
					String output = bufferedReader.readLine();
					if (output != null) {
						JSONArray restapiresponseArray = new JSONArray(output);
						if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
							List<Card> cards;
							Type listType = new TypeToken<List<Card>>() {
							}.getType();
							cards = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
							Card cardCreated = cards.get(0);
							logger.debug(CLASS_NAME + " addBadge() created BadgeDetails" + cardCreated);
							badgeInfo = processCardToBadgeInformation(cardCreated);
						}
					}
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_SUCCESS, false, "Badge added successfully !!!");
					provisioningResult.setBadgeInformation(badgeInfo);
				} else {
					logger.debug(CLASS_NAME + " addBadge() Error while provisioning badge in external system !!" + rs2Connection.getResponseMessage());
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "Exception while adding Badge in external system");
				}
			} else {
				if (_enableProvisioningWarnings) {
					logger.debug(CLASS_NAME + " addBadge() user does not exist ");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "User does not exist in external system");
				} else {
					logger.debug(CLASS_NAME + " addBadge() user does not exist , but warnings are disabled , returning as addBadge successfully in the system");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_SUCCESS, false, null);
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " addBadge(): Error during addBadge for user ", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CREATE_USER, IProvisoiningConstants.CREATE_USER_FAILURE, true, "Error during addBadge for user");
			provisioningResult.setBadgeInformation(badgeInfo);
		}
		logger.info(CLASS_NAME + " addBadge(): End of addBadge method");
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-04 - 4:44:43 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#addTempBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult addTempBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		return addBadge(requestNumber, roles, parameters, requestDetails, attMapping);
	}

	/**
	 * 
	 * soori 2019-12-02 - 5:38:47 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param deprovisionRole
	 * @param attMappin
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeAccess(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IProvisioningResult changeAccess(Long requestNumber, List provisionRoles, Map parameters, List requestDetails, List<IRoleInformation> deprovisionRoles, Map<String, String> attMapping) throws Exception {
		logger.debug(CLASS_NAME + " changeAccess(): Start of changeAccess method");
		IProvisioningResult provisioningResult = null;
		OutputStream outputStream = null;
		List<IRoleAuditInfo> roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
		try {
			if ((deprovisionRoles != null && !deprovisionRoles.isEmpty()) || (provisionRoles != null && !provisionRoles.isEmpty())) {
				if (parameters.get(externalUserIdAttribute) == null) {
					logger.debug(CLASS_NAME + " changeAccess() userId does not passed!!");
					provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "UserId does not passed!!");
				} else {
					String userIdToChangeAccess = parameters.get(externalUserIdAttribute).toString();
					CardHolder cardHolder = getUserByCardHolderId(userIdToChangeAccess);
					if (null != cardHolder) {
						List<CardholderAccessLevel> rolesInSystem = cardHolder.getCardholderAccessLevels();
						if (deprovisionRoles != null && !deprovisionRoles.isEmpty()) {
							Iterator<IRoleInformation> removeRoleItr = deprovisionRoles.iterator();
							while (removeRoleItr.hasNext()) {
								IRoleInformation roleInformation = (IRoleInformation) removeRoleItr.next();
								Map<String, List<String>> memberData = roleInformation.getMemberData();
								if (memberData != null && memberData.containsKey(IExtractionConstants.TECHNICAL_ROLE_NAME)) {
									List<String> flagValue = (List<String>) memberData.get(IExtractionConstants.TECHNICAL_ROLE_NAME);
									if (flagValue != null && flagValue.size() > 0) {
										rolesInSystem.removeIf(level -> level.getAccessLevelID().equals(flagValue.get(0)));
									}
								}
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.REMOVE_ROLE));
							}
						}
						if (provisionRoles != null && !provisionRoles.isEmpty()) {
							Iterator<IRoleInformation> addRoleItr = provisionRoles.iterator();
							while (addRoleItr.hasNext()) {
								CardholderAccessLevel cardholderAccessLevelToAdd = new CardholderAccessLevel();
								IRoleInformation roleInformation = (IRoleInformation) addRoleItr.next();

								// setting Role Id by reading from member data , which was set in getAll Roles
								// of Recon function
								Map<String, List<String>> memberData = roleInformation.getMemberData();
								if (memberData != null && memberData.containsKey(IExtractionConstants.TECHNICAL_ROLE_NAME)) {
									List<String> flagValue = (List<String>) memberData.get(IExtractionConstants.TECHNICAL_ROLE_NAME);
									if (flagValue != null && flagValue.size() > 0) {
										cardholderAccessLevelToAdd.setAccessLevelID(flagValue.get(0));
									}
								}
								// cardholderAccessLevelToAdd.setAccessLevelID(roleInformation.getName());

								// setting active date after formating
								if (null != roleInformation.getValidFrom()) {
									cardholderAccessLevelToAdd.setActivateDate(parseDateRS2StringFormat(roleInformation.getValidFrom()));
								}
								// setting deactive date after formating
								if (null != roleInformation.getValidTo()) {
									cardholderAccessLevelToAdd.setDeactivateDate(parseDateRS2StringFormat(roleInformation.getValidTo()));
								}
								rolesInSystem.add(cardholderAccessLevelToAdd);
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.ADD_ROLE));
							}
						}

						cardHolder.setCardholderAccessLevels(rolesInSystem);
						String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat("/").concat(userIdToChangeAccess);
						RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
						Gson gson = new Gson();
						String gsonString = gson.toJson(cardHolder);
						logger.debug(CLASS_NAME + "   changeAccess()   request payload  :::  " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
						} else {
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error assigning roles");
						}
					} else {
						if (_enableProvisioningWarnings) {
							logger.debug(CLASS_NAME + " changeAccess() user does not exist ");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "User does not exist in external system");
						} else {
							logger.debug(CLASS_NAME + " changeAccess() user does not exist , but warnings are disabled , returning as change Access successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, null);
						}
					}
				}
			} else {
				provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_SUCCESS, false, "Access changed successfully.");
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " changeAccess(): Error Assigning the roles = ", e);
			roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.ASSIGN_ROLES_FAILURE, true, "Error assigning roles");
		}
		provisioningResult.setRoleList(roleAuditInfoList);
		return provisioningResult;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 1:39:21 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return IProvisioningResult
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult changeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " changeBadge(): Start of changeBadge method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "Error during change Badge");
		IBadgeInformation badgeInfo = new BadgeInformation();
		List<Card> cards = null;
		OutputStream outputStream = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " changeBadge() userId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "UserId does not passed!!");
			}
			if (parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER) == null) {
				logger.debug(CLASS_NAME + " changeBadge() badgeId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "badgeId does not passed!!");
			} else {
				String userId = parameters.get(externalUserIdAttribute).toString();
				String cardNumber = "";
				if (_facilitycodeAssociatedWithBadgeId) {
					cardNumber = getCardNumberFromComboCode(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString());
				} else {
					cardNumber = parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString();
				}
				cards = getCardByUserAndNumber(userId, cardNumber, null);
				if (null != cards && !cards.isEmpty()) {
					logger.debug(CLASS_NAME + " changeBadge() user badge provisioned in the system, proceed to deactivate badge");
					Card cardToChange = cards.get(0);
					badgeInfo = processCardToBadgeInformation(cards.get(0));
					String cardIdtoChange = cardToChange.getCardId();
					String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS).concat("/").concat(cardIdtoChange);
					RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
					Gson gson = new Gson();
					if (parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE) != null) {
						cardToChange.setActiveDate(parseDateStringToDateString(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_ACTIVE_DATE).toString(), _alertAppDateFormat, RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT));
					}
					if (parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE) != null) {
						cardToChange.setExpireDate(parseDateStringToDateString(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_EXPIRY_DATE).toString(), _alertAppDateFormat, RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT));
					}
					String gsonString = gson.toJson(cardToChange);
					logger.debug(CLASS_NAME + "   changeBadge()   request payload  :::  " + gsonString);
					outputStream = rs2Connection.getOutputStream();
					outputStream.write(gsonString.getBytes());
					outputStream.flush();
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						logger.debug(CLASS_NAME + " changeBadge() badge changed successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, "badge changed successfully in the system");
						provisioningResult.setBadgeInformation(badgeInfo);
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " changeBadge() badge does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "badge does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " changeBadge() badge does not exist , but warnings are disabled , returning as changed successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
						provisioningResult.setBadgeInformation(badgeInfo);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " changeBadge(): Error during activating user badge", e);
			logger.error(CLASS_NAME + " changeBadge()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "Error during changing a user badge");
			provisioningResult.setBadgeInformation(badgeInfo);
		}
		logger.info(CLASS_NAME + " changeBadge(): End of changeBadge method");
		return provisioningResult;

	}

	/**
	 * 
	 * soori - 2019-12-06 - 2:33:51 pm
	 * 
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 * @param arg4
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#changeBadgeRoles(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IProvisioningResult changeBadgeRoles(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " changeBadgeRoles(): Start of changeBadgeRoles method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.CHANGE_USER_FAILURE, null, "Error during changeBadgeRoles");
		;
		List<IRoleAuditInfo> roleAuditInfoList = new ArrayList<IRoleAuditInfo>();
		IBadgeInformation badgeInfo = new BadgeInformation();
		List<Card> cards = null;
		OutputStream outputStream = null;
		try {
			logger.debug(CLASS_NAME + " changeBadgeRoles() :: parameters" + parameters);
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " changeBadgeRoles() userId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "UserId does not passed!!");
			}
			if (parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER) == null) {
				logger.debug(CLASS_NAME + " changeBadgeRoles() badgeId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "badgeId does not passed!!");
			} else {
				String userId = parameters.get(externalUserIdAttribute).toString();
				String cardNumber = "";
				if (_facilitycodeAssociatedWithBadgeId) {
					cardNumber = getCardNumberFromComboCode(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString());
				} else {
					cardNumber = parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString();
				}
				cards = getCardByUserAndNumber(userId, cardNumber, null);
				if (null != cards && !cards.isEmpty() && roles != null && !roles.isEmpty()) {
					logger.debug(CLASS_NAME + " changeBadgeRoles() user badge provisioned in the system, proceed to changeBadgeRoles");
					Card cardToChange = cards.get(0);
					badgeInfo = processCardToBadgeInformation(cards.get(0));
					String cardIdtoChange = cardToChange.getCardId();
					Card cardstoUpdate = getCardByCardId(cardIdtoChange);
					if (null != cardstoUpdate) {
						List<CardAccessLevel> existingRolesInSystem = cardstoUpdate.getCardAccessLevels();
						Iterator<IRoleInformation> addRoleItr = roles.iterator();
						while (addRoleItr.hasNext()) {
							IRoleInformation roleInformation = (IRoleInformation) addRoleItr.next();
							if (IProvisoiningConstants.REMOVE_ROLE.equals(roleInformation.getRoleAction())) {
								Map<String, List<String>> memberData = roleInformation.getMemberData();
								if (memberData != null && memberData.containsKey(IExtractionConstants.TECHNICAL_ROLE_NAME)) {
									List<String> flagValue = (List<String>) memberData.get(IExtractionConstants.TECHNICAL_ROLE_NAME);
									if (flagValue != null && flagValue.size() > 0) {
										existingRolesInSystem.removeIf(level -> level.getAccessLevelID().equals(flagValue.get(0)));
									}
								}
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.REMOVE_ROLE));
							}
							if (IProvisoiningConstants.ADD_ROLE.equals(roleInformation.getRoleAction())) {
								CardAccessLevel CardAccessLevelToAdd = new CardAccessLevel();
								Map<String, List<String>> memberData = roleInformation.getMemberData();
								if (memberData != null && memberData.containsKey(IExtractionConstants.TECHNICAL_ROLE_NAME)) {
									List<String> flagValue = (List<String>) memberData.get(IExtractionConstants.TECHNICAL_ROLE_NAME);
									if (flagValue != null && flagValue.size() > 0) {
										CardAccessLevelToAdd.setAccessLevelID(flagValue.get(0));
									}
								}
								// cardholderAccessLevelToAdd.setAccessLevelID(roleInformation.getName());
								// TODO:: CHECK WITH ADEPU :: FOUND ISSUE WHILE TESTING WITh SERVER.CARD ROLES SHOULD NOT HAVE BOTH DATES (THEY ARE NOT VISIBLE IN UI)
								// This is worked for User Roles , tested with more than 15 users (all permutations and combinations)
								if (null != roleInformation.getValidFrom()) {
									CardAccessLevelToAdd.setActivateDate(parseDateRS2StringFormat(roleInformation.getValidFrom()));
								}
								if (null != roleInformation.getValidTo()) {
									CardAccessLevelToAdd.setDeactivateDate(parseDateRS2StringFormat(roleInformation.getValidTo()));
								}
								existingRolesInSystem.add(CardAccessLevelToAdd);
								roleAuditInfoList.add(getRoleAuditInfo(roleInformation, IProvisoiningConstants.ADD_ROLE));
							}
						}
						cardstoUpdate.setCardAccessLevels(existingRolesInSystem);
						String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS).concat("/").concat(cardIdtoChange);
						RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_PUT, _apiUserName, _apiPassword, _publicKey);
						Gson gson = new Gson();
						String gsonString = gson.toJson(cardstoUpdate);
						logger.debug(CLASS_NAME + "   changeBadgeRoles()   request payload  :::  " + gsonString);
						outputStream = rs2Connection.getOutputStream();
						outputStream.write(gsonString.getBytes());
						outputStream.flush();
						if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
							logger.debug(CLASS_NAME + " changeBadgeRoles() badge changeBadgeRoles successfully in the system");
							provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_USER, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, "badge changeBadgeRoles successfully in the system");
							provisioningResult.setBadgeInformation(badgeInfo);
						}

					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " changeBadgeRoles() badge does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.CHANGE_USER_FAILURE, true, "badge does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " changeBadgeRoles() badge does not exist , but warnings are disabled , returning as changeBadgeRoles successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.CHANGE_USER_SUCCESS, false, null);
						provisioningResult.setBadgeInformation(badgeInfo);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " changeBadgeRoles(): Error changeBadgeRoles to user badge ", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_CHANGE_ROLES, IProvisoiningConstants.CHANGE_USER_FAILURE, null, "Error during changeBadgeRoles");
			provisioningResult.setBadgeInformation(badgeInfo);
		}
		logger.info(CLASS_NAME + " changeBadgeRoles(): End of changeBadgeRoles method");
		return provisioningResult;
	}

	/**
	 * 
	 * soori 2019-12-04 - 5:07:00 pm
	 * 
	 * @param requestNumber
	 * @param roles
	 * @param parameters
	 * @param requestDetails
	 * @param attMapping
	 * @return
	 * @throws Exception
	 * @see com.alnt.access.provisioning.services.IConnectionInterface#removeBadge(java.lang.Long, java.util.List, java.util.Map, java.util.List, java.util.Map)
	 *
	 *
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public IProvisioningResult removeBadge(Long requestNumber, List roles, Map parameters, List requestDetails, Map<String, String> attMapping) throws Exception {
		logger.info(CLASS_NAME + " removeBadge(): Start of removeBadge method");
		IProvisioningResult provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "Error deleting Badge in external system");
		IBadgeInformation badgeInfo = new BadgeInformation();
		List<Card> cards = null;
		try {
			if (parameters.get(externalUserIdAttribute) == null) {
				logger.debug(CLASS_NAME + " removeBadge() userId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "userId does not passed!!");
			}
			if (parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER) == null) {
				logger.debug(CLASS_NAME + " removeBadge() badgeId does not passed!!");
				return prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "badgeId does not passed!!");
			} else {
				String userId = parameters.get(externalUserIdAttribute).toString();
				String cardNumber = "";
				if (_facilitycodeAssociatedWithBadgeId) {
					cardNumber = getCardNumberFromComboCode(parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString());
				} else {
					cardNumber = parameters.get(RS2AccessItConnectorConstants.ATTR_BADGE_NUMBER).toString();
				}
				cards = getCardByUserAndNumber(userId, cardNumber, null);
				if (null != cards && !cards.isEmpty()) {
					logger.debug(CLASS_NAME + " removeBadge() user badge provisioned in the system, proceed to deactivate badge");
					Card cardToDelete = cards.get(0);
					badgeInfo = processCardToBadgeInformation(cards.get(0));
					String cardIdtoDelete = cardToDelete.getCardId();
					String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS).concat("/").concat(cardIdtoDelete);
					RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_DELETE, _apiUserName, _apiPassword, _publicKey);
					if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
						logger.debug(CLASS_NAME + " removeBadge() badge deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, "badge deleted successfully in the system");
						provisioningResult.setBadgeInformation(badgeInfo);
					}
				} else {
					if (_enableProvisioningWarnings) {
						logger.debug(CLASS_NAME + " removeBadge() badge does not exist in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "badge does not exist in external system");
					} else {
						logger.debug(CLASS_NAME + " removeBadge() badge does not exist , but warnings are disabled , returning as deleted successfully in the system");
						provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_SUCCESS, false, null);
						provisioningResult.setBadgeInformation(badgeInfo);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " removeBadge(): Error during removing user badge ", e);
			logger.error(CLASS_NAME + " removeBadge()", e);
			provisioningResult = prepareProvisioningResult(IProvisoiningConstants.PROV_ACTION_DELETE_USER, IProvisoiningConstants.DELETE_USER_FAILURE, true, "Error removing a user badge:");
			provisioningResult.setBadgeInformation(badgeInfo);
		}
		logger.info(CLASS_NAME + " removeBadge(): End of removeBadge method");
		return provisioningResult;
	}

	/**
	 * soori 2019-12-04 - 5:17:46 pm
	 * 
	 * @param userId
	 * @param cardNumber
	 * @param lastModified
	 * 
	 *                     valid combinations
	 * 
	 *                     getCardByUserAndNumber("userId", 123,null) -- card of user with card number -- LOCK /UNLOCK/UPDATE /API
	 * 
	 *                     getCardByUserAndNumber("userId", null,null) -- all cards of user --FULl RECON
	 * 
	 *                     getCardByUserAndNumber("userId", null,"2019-09-09T09:09:09") -- specific user cards modified after given date -- incremental Recon
	 * 
	 *                     getCardByUserAndNumber(null, null,"2019-09-09T09:09:09") -- all cards modified after a given date time -- Incremental recon
	 * 
	 * @return List<Card>
	 *
	 */
	private List<Card> getCardByUserAndNumber(String userId, String cardNumber, String lastModified) throws Exception {
		List<Card> cards = null;
		String filter = "?filter=";
		boolean hasUser = false;
		if (null != userId && !userId.isEmpty()) {
			logger.debug(CLASS_NAME + " getCardByUserAndNumber():  cards with  userId :: " + userId);
			filter = filter.concat("CardholderId eq ").concat(userId);
			hasUser = true;
		}
		if (null != cardNumber && !cardNumber.isEmpty()) {
			logger.debug(CLASS_NAME + " getCardByUserAndNumber():  cards with  cardNumber :: " + cardNumber);
			filter = filter.concat(" and ").concat("cardnumber eq ").concat(cardNumber);
		}

		if (null != lastModified && !lastModified.isEmpty()) {
			logger.debug(CLASS_NAME + " getCardByUserAndNumber():  cards with  cardNumber :: " + cardNumber);
			if (hasUser) {
				filter = filter.concat(" and ").concat("lastmodified gt datetime'").concat(lastModified).concat("'");
			} else {
				filter = filter.concat("lastmodified gt datetime'").concat(lastModified).concat("'");
			}

		}

		String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS).concat(filter);
		RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
		if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
			String output = bufferedReader.readLine();
			if (output != null) {
				JSONArray restapiresponseArray = new JSONArray(output);
				if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
					Type listType = new TypeToken<List<Card>>() {
					}.getType();
					cards = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
				}
			}
		}
		return cards;
	}

	/**
	 * 
	 * soori 2019-12-06 - 12:01:43 pm
	 * 
	 * @param roleInformation
	 * @param action
	 * @return IRoleAuditInfo
	 *
	 */
	private IRoleAuditInfo getRoleAuditInfo(IRoleInformation roleInformation, String action) {
		IRoleAuditInfo roleAuditInfo = new RoleAuditInfo();
		roleAuditInfo.setRoleName(roleInformation.getName());
		roleAuditInfo.setValidFrom(roleInformation.getValidFrom());
		roleAuditInfo.setValidTo(roleInformation.getValidTo());
		roleAuditInfo.setAction(action);
		return roleAuditInfo;
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:55:00 am
	 * 
	 * @param dateString
	 * @param currentFormat
	 * @param exceptedFormat
	 * @return String
	 * @throws Exception
	 *
	 */
	private String parseDateStringToDateString(String dateString, String currentFormat, String exceptedFormat) throws Exception {
		try {
			logger.debug(CLASS_NAME + " parseDateString() dateString to parse :: " + dateString);
			logger.debug(CLASS_NAME + " parseDateString() dateString  current format :: " + dateString);
			logger.debug(CLASS_NAME + " parseDateString() dateString  exceptedFormat :: " + exceptedFormat);
			DateTimeFormatter oldPattern = DateTimeFormatter.ofPattern(currentFormat);
			DateTimeFormatter newPattern = DateTimeFormatter.ofPattern(exceptedFormat);
			LocalDateTime datetime = LocalDateTime.parse(dateString, oldPattern);
			return datetime.format(newPattern);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:55:08 am
	 * 
	 * @param date
	 * @param dateFormat
	 * @return Date
	 * @throws ParseException
	 *
	 */
	private static final Date parseStringToDate(String date, String dateFormat) throws ParseException {
		if (dateFormat == null || dateFormat.equals("")) {
			dateFormat = RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT;
		}
		SimpleDateFormat formater = new SimpleDateFormat(dateFormat);
		return formater.parse(date);
	}

	/**
	 * 
	 * soori 2019-12-06 - 11:55:26 am
	 * 
	 * @param date
	 * @return String
	 * @throws ParseException
	 *
	 */
	private static final String parseDateRS2StringFormat(Date date) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT);
		return dateFormat.format(date);
	}

	/**
	 * soori 2019-12-04 - 6:01:17 pm
	 * 
	 * @param cardCreated
	 * @return IBadgeInformation
	 * @throws ParseException
	 *
	 */
	private IBadgeInformation processCardToBadgeInformation(Card cardCreated) throws ParseException {
		IBadgeInformation badgeInfo;
		badgeInfo = new BadgeInformation();

		if (_facilitycodeAssociatedWithBadgeId) {
			badgeInfo.setBadgeId(cardCreated.getFacilityCode().toString().concat(_separator).concat(cardCreated.getCardNumber().toString()));
		} else {
			badgeInfo.setBadgeId(cardCreated.getCardNumber().toString());
		}
		if (null != cardCreated.getActiveDate() && !cardCreated.getActiveDate().isEmpty()) {
			badgeInfo.setValidFrom(parseStringToDate(cardCreated.getActiveDate(), null));
		}
		if (null != cardCreated.getExpireDate() && !cardCreated.getExpireDate().isEmpty()) {
			badgeInfo.setValidTo(parseStringToDate(cardCreated.getExpireDate(), ""));
		}
		badgeInfo.setBadgeStatus(cardCreated.getCardStatus().toString());
		badgeInfo.setBadgeStatus(STATUS_MAP.get(cardCreated.getCardStatus()));
		if (null != cardCreated.getBadgeType() && !cardCreated.getBadgeType().isEmpty()) {
			badgeInfo.setBadgeType(cardCreated.getBadgeType());
			badgeInfo.setBadgeTypeStr(cardCreated.getBadgeType());
		}
		badgeInfo.setProvAction(IProvisoiningConstants.PROV_ACTION_CREATE_USER);
		if (null != cardCreated.getNotes() && !cardCreated.getNotes().isEmpty()) {
			badgeInfo.setDescription(cardCreated.getNotes());
		}
		return badgeInfo;
	}

	/**
	 * soori 2019-12-04 - 12:10:21 am
	 * 
	 * @param cardToCreate
	 *
	 */
	private void validateAndFormatOptionalData(Card cardToCreate) throws Exception {
		if (null != cardToCreate.getActiveDate() && !cardToCreate.getActiveDate().isEmpty()) {
			cardToCreate.setActiveDate(parseDateStringToDateString(cardToCreate.getActiveDate(), _alertAppDateFormat, RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT));
		}
		if (null != cardToCreate.getExpireDate() && !cardToCreate.getExpireDate().isEmpty()) {
			cardToCreate.setExpireDate(parseDateStringToDateString(cardToCreate.getExpireDate(), _alertAppDateFormat, RS2AccessItConnectorConstants.DEFAULT_DATE_FORMAT));
		}

		if (null != cardToCreate.getIpLocksetUserType()) {
			if (cardToCreate.getIpLocksetUserType() < 1 || cardToCreate.getIpLocksetUserType() > 10) {
				throw new Exception("IpLocksetUserType should be between 1 and 10");
			}
		}
		if (null != cardToCreate.getIpLocksetAccessMode()) {
			if (cardToCreate.getIpLocksetAccessMode() < 1 || cardToCreate.getIpLocksetAccessMode() > 4) {
				throw new Exception("IpLocksetAccessMode should be between 1 and 4");
			}
		}
		if (null != cardToCreate.getCardStatus()) {
			if (cardToCreate.getCardStatus() < 0 || cardToCreate.getCardStatus() > 4) {
				throw new Exception("CardStatus should be between 0 and 2");
			}
		}

	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:30:05 pm
	 * 
	 * @param cardToCreate
	 * @return
	 *
	 */
	private boolean validateMandatoryCardData(Card cardToCreate) {
		boolean isCardValid = true;
		/*
		 * if(null == cardToCreate.getCardholderId()) { isCardValid = false; logger.error( CLASS_NAME+" validateCardData(): cardholder(user)Id is required "); }
		 */
		if (null == cardToCreate.getCardNumber()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): Card Number is required ");
		}
		if (null == cardToCreate.getFacilityCode()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): FacilityCode is required ");
		}
		if (null == cardToCreate.getPinNumber()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): Pin Number is required ");
		}
		if (null == cardToCreate.getPinExempt()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): PinExcempt is required ");
		}
		if (null == cardToCreate.getApbExempt()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): ApbExempt is required ");
		}
		if (null == cardToCreate.getUseCustomReporting()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): UseCustomReporting is required ");
		}
		if (null == cardToCreate.getUseExtendedAccessTimes()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): UseExtendedAccessTimes is required ");
		}
		if (null == cardToCreate.getCardStatus()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): CardStatus is required ");
		}
		if (null == cardToCreate.getUserLevel()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): UserLevel is required ");
		}
		if (null == cardToCreate.getIssueLevel()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): IssueLevel is required ");
		}
		if (null == cardToCreate.getUseCount()) {
			isCardValid = false;
			logger.error(CLASS_NAME + " validateCardData(): UseCount is required ");
		}
		return isCardValid;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:42:16 pm
	 * 
	 * @param provisioningAction
	 * @param msgCode
	 * @param isFailed
	 * @param msgDesc
	 * @return IProvisioningResult
	 *
	 */
	private IProvisioningResult prepareProvisioningResult(String provisioningAction, String msgCode, Boolean isFailed, String msgDesc) {
		IProvisioningResult provisioningResult = new ProvisioningResult();
		provisioningResult.setMsgCode(msgCode);
		if (null != isFailed) {
			provisioningResult.setProvFailed(isFailed);
		}
		provisioningResult.setMsgDesc(msgDesc);
		provisioningResult.setProvAction(provisioningAction);
		return provisioningResult;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:14:29 pm
	 * 
	 * @param userAttributesMap
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException 
	 *
	 */
	@SuppressWarnings({ "rawtypes" })
	public CardHolder prepareCardHolder(Map userAttributesMap) {
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		JsonElement jsonElement = gson.toJsonTree(userAttributesMap);
		CardHolder cardHolder = gson.fromJson(jsonElement, CardHolder.class);
		if (null != userAttributesMap.get(RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA)) {
			if (userAttributesMap.get(RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA) instanceof byte[]) {
				CardHolderImage cardHolderImage = new CardHolderImage();
				byte[] array = (byte[]) userAttributesMap.get(RS2AccessItConnectorConstants.ATTR_USER_IMAGE_DATA);
				String userImage = Base64.getEncoder().encodeToString(array);
				cardHolderImage.setImageData(userImage);
				if (null != userAttributesMap.get(RS2AccessItConnectorConstants.ATTR_USER_IMAGETYPE)) {
					cardHolderImage.setImageType(Integer.parseInt(userAttributesMap.get(RS2AccessItConnectorConstants.ATTR_USER_IMAGETYPE).toString()));
				} else {
					cardHolderImage.setImageType(1);
				}
				List<CardHolderImage> images = new ArrayList<CardHolderImage>();
				images.add(cardHolderImage);
				cardHolder.setImages(images);
			}
		}
		CardHolderUserColumns userColumns = gson.fromJson(jsonElement, CardHolderUserColumns.class);
		// TODO :: need implicit check for Numeric fields , and date conversion for Date fields , it required custom code ( no generic)
		cardHolder.setUserColumns(userColumns);
		return cardHolder;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:30:24 pm
	 * 
	 * @param userAttributesMap
	 * @return Card
	 *
	 */
	@SuppressWarnings({ "rawtypes" })
	private Card prepareCard(Map userAttributesMap) {
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();
		JsonElement jsonElement = gson.toJsonTree(userAttributesMap);
		Card card = gson.fromJson(jsonElement, Card.class);
		return card;
	}

	/**
	 * 
	 * soori - 2019-12-06 - 12:39:47 pm
	 * 
	 * @param userId
	 * @return CardHolder
	 * @throws Exception
	 *
	 */
	private CardHolder getUserByCardHolderId(String userId) throws Exception {
		CardHolder cardHolder = null;
		try {
			String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDHOLDERS).concat("/").concat(userId);
			RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
			if (rs2Connection.getResponseCode() != HttpStatus.SC_OK) {
				logger.debug(CLASS_NAME + " getUserByCardHolderId():  user with  userId :: " + userId);
				logger.error(CLASS_NAME + " getUserByCardHolderId()   " + rs2Connection.getResponseMessage());
				return null;
			} else if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
				String output = bufferedReader.readLine();
				if (output != null) {
					JSONArray restapiresponseArray = new JSONArray(output);
					if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
						List<CardHolder> cardHolders;
						Type listType = new TypeToken<List<CardHolder>>() {
						}.getType();
						cardHolders = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
						cardHolder = cardHolders.get(0);
						logger.debug(CLASS_NAME + " getUserByCardHolderId():  cardHolder:: " + cardHolder);
					}
				}
			}
		} catch (Exception e) {
			logger.error(CLASS_NAME + " getUserByCardHolderId(): Error while call to external system", e);
			throw e;
		}
		return cardHolder;
	}

	private Card getCardByCardId(String cardId) throws Exception {
		Card card = null;
		String endPoint = _baseUrl.concat(RS2AccessItConnectorConstants.ENDPOINT_CARDS).concat("/").concat(cardId);
		RS2AccessItURLConnection rs2Connection = RS2AccessItClientHelper.getConnection(endPoint, RS2AccessItConnectorConstants.REQ_METHOD_TYPE_GET, _apiUserName, _apiPassword, _publicKey);
		if (rs2Connection.getResponseCode() == HttpStatus.SC_OK) {
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((rs2Connection.getInputStream())));
			String output = bufferedReader.readLine();
			if (output != null) {
				JSONArray restapiresponseArray = new JSONArray(output);
				if (null != restapiresponseArray && restapiresponseArray.length() > 0) {
					List<Card> cards;
					Type listType = new TypeToken<List<Card>>() {
					}.getType();
					cards = new Gson().fromJson(String.valueOf(restapiresponseArray), listType);
					card = cards.get(0);
					logger.debug(CLASS_NAME + " getCardByCardId():  card:: " + card);
				}
			}
		}
		return card;
	}

	private String getCardNumberFromComboCode(String cardId) throws Exception {
		if (cardId.contains(_separator)) {
			int index = cardId.indexOf(_separator);
			return cardId.substring(index + 1).trim();
		}
		return cardId;
	}

	private String getFacilityCodeFromComboCode(String cardId) throws Exception {
		if (cardId.contains(_separator)) {
			int index = cardId.indexOf(_separator);
			return cardId.substring(0, index).trim();
		}
		return cardId;
	}

	@Override
	public void setTaskId(Long arg0) {
	}

	@Override
	public IProvisioningResult updatePassword(Long arg0, String arg1, String arg2, String arg3) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getBadgeLastLocation(String arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getDetailsAsList(String arg0, String arg1) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List getExistingBadges(String arg0) throws Exception {

		return null;
	}

	@Override
	public IProvisioningStatus getProvisioningStatus(Map<String, String> arg0) throws Exception {

		return null;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List provision(Long arg0, String arg1, List arg2, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg3, Map arg4, List arg5, List<com.alnt.fabric.component.rolemanagement.search.IRoleInformation> arg6,
			Map<String, String> arg7) throws Exception {
		return null;
	}

	@SuppressWarnings("rawtypes")
	public IProvisioningResult create(Long arg0, List arg1, Map arg2, List arg3, Map<String, String> arg4, Map<String, String> arg5) throws Exception {
		return null;
	}
	// PROVISIONING OPERATIONS :: BEGIN

}
